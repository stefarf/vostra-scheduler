#!/usr/bin/env bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
cd $DIR/..
REPO=$PWD

cd $REPO/go/cmd/vsch
#go build

function build() {
  GOOS=$1
  GOARCH=$2
  env GOOS=$1 GOARCH=$2 go build -o $REPO/bin/vsch-$1-$2
}

build linux amd64
build darwin arm64
