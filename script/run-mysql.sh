#!/usr/bin/env bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
cd $DIR/..
REPO=$PWD

DATA=$REPO/data/mysql
mkdir -p $DATA

docker stop mariadb
docker rm mariadb
docker stop mysql
docker rm mysql

docker run \
  --detach \
  --name mysql \
  --env TZ="Asia/Jakarta" \
  --env MYSQL_ROOT_PASSWORD="secret" \
  -v $DATA:/var/lib/mysql \
  -p 3306:3306 \
  mysql:5.7.38
