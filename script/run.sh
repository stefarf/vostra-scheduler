#!/usr/bin/env bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
cd $DIR/..
REPO=$PWD

export CONFIG_FILE=$REPO/config.json
export CWD=$REPO
#$REPO/go/cmd/vsch/vsch -cfg
$REPO/go/cmd/vsch/vsch $* 2>&1 | tee $REPO/run.log
