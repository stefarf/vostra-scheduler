#!/usr/bin/env bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
cd $DIR/..
REPO=$PWD
set -a && source $REPO/env/.env && set +a
set -e

cd $REPO/sample-app
fnm use 16.14.0
npm install
npm run build

export CONFIG_FILE=$REPO/config.json
export CWD=$REPO

rm -rf $REPO/sample-app/dump
#cd $REPO/go/cmd/vsch && go run . -cfg
cd $REPO/go/cmd/vsch && go run . $* 2>&1 | tee $REPO/run.log
