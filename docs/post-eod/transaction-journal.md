# Job Distribution model for Post EOD (transaction journal)

## Example

### Sequence 1: Create jobs

Triggered by time: at 0am daily.

Create job to collect yesterday's data in 10 minutes slice.

```js
async function createJobs(/* no input */) {
    const jobs = [];

    // create jobs logic ...
    return jobs;
}

app.post('/posteod/create-jobs', async (req, res) => {
    const jobs = await createJobs()
    res.send(jobs)
})
```

Output:

```json
[
  {
    "JobId": "001",
    "Date": "2022-04-10",
    "StartMinute": 0,
    "IntervalMinute": 10
  },
  {
    "JobId": "002",
    "Date": "2022-04-10",
    "StartMinute": 10,
    "IntervalMinute": 10
  },
  {
    "JobId": "..."
  }
]
```

Total: 6 x 24 jobs = 144 jobs

### Sequence 2: Collect, filter and convert data

Example payload for job `001` :

```json
{
  "Job": {
    "JobId": "001",
    "Date": "2022-04-10",
    "StartMinute": 0,
    "IntervalMinute": 10
  },
  "TotalJobs": 144
}
```

Action:

1. Call Mambu API to collect journal data. (ex: 5000 data)
2. Filter journal data. (ex: 4900 data)
3. Convert to predefined format. (ex: 4900 data)

```js
async function collectFilterConvert(jobPayload) {
    const result = {}
    // process
    return result
}

app.post('/posteod/collect-filter-convert', async (req, res) => {
    const input = {
        // ...
    }
    const output = await collectFilterConvert(input)
    res.send(output)
})
```

Output:

```json
{
  "Job": {
    "JobId": "001",
    "Date": "2022-04-10",
    "StartMinute": 0,
    "IntervalMinute": 10,
    "Result": [
      [
        "example",
        "row",
        "output",
        "..."
      ],
      [
        "..."
      ]
    ]
  },
  "TotalJobs": 144
}
```

### Sequence 3: Sink to DB

```mysql
CREATE TABLE IF NOT EXISTS `temp_entries`
(
    `job_id`          INT(11)     NOT NULL AUTO_INCREMENT,
    `TBUBRCP`         VARCHAR(45)   DEFAULT NULL,
    `TBUSBCH`         VARCHAR(45)   DEFAULT NULL,
    `TBUDOCM`         VARCHAR(45)   DEFAULT NULL,
    `TBUSEQN`         INT(11)       DEFAULT NULL,
    `TBUBRCO`         VARCHAR(45)   DEFAULT NULL,
    `TBUDATE`         VARCHAR(45)   DEFAULT NULL,
    `TBUTIME`         VARCHAR(45)   DEFAULT NULL,
    `TBUPODT`         VARCHAR(45)   DEFAULT NULL,
    `TBUREFN`         VARCHAR(45)   DEFAULT NULL,
    `TBUCHRT`         VARCHAR(45)   DEFAULT NULL,
    `TBUDSC1`         VARCHAR(225)  DEFAULT NULL,
    `TBUACB1`         VARCHAR(45)   DEFAULT NULL,
    `TBUACN1`         VARCHAR(45)   DEFAULT NULL,
    `TBUTRC1`         INT(11)       DEFAULT NULL,
    `TBUDBC1`         VARCHAR(45)   DEFAULT NULL,
    `TBUCYCO`         VARCHAR(45)   DEFAULT NULL,
    `TBUAMNT`         DOUBLE(20, 2) DEFAULT NULL,
    `TBUEXRT`         INT(11)       DEFAULT NULL,
    `TBUTYPE`         VARCHAR(45)   DEFAULT NULL,
    `TBUUSIN`         VARCHAR(45)   DEFAULT NULL,
    `TBUDSIN`         VARCHAR(45)   DEFAULT NULL,
    `TBUDTIN`         VARCHAR(45)   DEFAULT NULL,
    `TBUTMIN`         VARCHAR(45)   DEFAULT NULL,
    `idempotency_key` VARCHAR(45) NOT NULL,
    `type`            INT(11)     NOT NULL,
    `creation_date`   DATETIME      DEFAULT NULL,
    `booking_date`    DATETIME      DEFAULT NULL,
    `product_key`     VARCHAR(225)  DEFAULT NULL,
    `report_type`     VARCHAR(1)    DEFAULT NULL,
    `DEAL_REF`        VARCHAR(45)   DEFAULT NULL,
    `CIF_NO`          VARCHAR(45)   DEFAULT NULL,
    `product_id`      VARCHAR(45)   DEFAULT NULL,
    `user_id`         VARCHAR(45)   DEFAULT NULL,
    `user_trx`        VARCHAR(45)   DEFAULT NULL,
    PRIMARY KEY (`job_id`),
    KEY `type_idx` (`type`),
    KEY `key_idx` (`idempotency_key`),
    KEY `TBUREFN_idx` (`TBUREFN`),
    KEY `TBUACN1_idx` (`TBUACN1`)
) ENGINE = InnoDB;
```

Input from previous stage:

```json
{
  "Job": {
    "JobId": "001",
    "Date": "2022-04-10",
    "StartMinute": 0,
    "IntervalMinute": 10,
    "Result": [
      [
        "example",
        "row",
        "output",
        "..."
      ],
      [
        "..."
      ]
    ]
  },
  "TotalJobs": 144
}
```

Transformation function:

```js
function transform(input) {
    rows = [];
    input.Job.Result.forEach(row => {
        const [
            TBUBRCP,
            TBUSBCH,
            TBUDOCM,
            TBUSEQN,
            // ...
        ] = row;
        rows.push({
            JobId: input.Job.JobId,
            TBUBRCP,
            TBUSBCH,
            TBUDOCM,
            TBUSEQN,
            // ...
        })
    })
    return rows;
}

app.post('/posteod/transform', async (req, res) => {
    const input = {
        // ...
    }
    const output = await transform(input)
    res.send(output)
})

```

Output:

```json
[
  {
    "JobId": "001",
    "TBUBRCP": "...",
    "TBUSBCH": "...",
    "TBUDOCM": "...",
    "TBUSEQN": "..."
  },
  {}
]
```

Scheduler will stored in DB using following insert for each row:

```
INSERT INTO table_name (job_id, tbubrcp, tbusbch)
VALUES ("001", "...", "...")
```

It will count if total number of processed jobs already 144, then it will trigger the next sequence to run.

### Sequence 4: Summarize, SFTP and email

No input from previous stage. The scheduler will trigger this sequence to run if the previous sequence has done sinking
all data to DB.

```js
function sequence(/* no input */) {
    // query db to summarize, SFTP and email
}
```

## Configuration

In-file configuration:

```json
{
  "Process": {
    "Shell": [
      "node",
      "post-eod-gl/dist/server.js"
    ]
  },
  "MySQL": {
  },
  "Function": {
    "post_eod_transaction_journal:create_jobs": {
      "Type": "emitter",
      "Emitter": {
        "TriggerBy": "cron",
        "Cron": [
          "* * * ..."
        ]
      },
      "Handler": {
        "Path": "/posteod/create-jobs"
      }
    },
    "post_eod_transaction_journal:collect_filter_convert": {
      "Type": "processor",
      "Handler": {
        "Path": "/posteod/collect-filter-convert"
      }
    },
    "post_eod_transaction_journal:sink_db": {
      "Type": "sink-db",
      "SinkDb": {
        "CreateTable": " ... DDL ... "
      },
      "Handler": {
        "Path": "/posteod/transform"
      }
    },
    "post_eod_transaction_journal:gl_extraction_report": {
      "Type": "trigger",
      "Handler": {}
    },
    "post_eod_transaction_journal:recon_post_eod_report": {
      "Type": "trigger",
      "Handler": {}
    }
  }
}
```

In-DB configuration:

```json
{
  "Flow": [
    {
      "From": "post_eod_transaction_journal:create_jobs",
      "To": "post_eod_transaction_journal:collect_filter_convert",
      "Type": "jobs"
    },
    {
      "From": "post_eod_transaction_journal:collect_filter_convert",
      "To": "post_eod_transaction_journal:sink_db",
      "Type": "data"
    },
    {
      "From": "post_eod_transaction_journal:sink_db",
      "ToMany": [
        "post_eod_transaction_journal:gl_extraction_report",
        "post_eod_transaction_journal:recon_post_eod_report"
      ],
      "Trigger": {
        "By": "number-of-execution",
        "Field": "TotalJobs"
      }
    }
  ]
}
```