module.exports = {
  roots: ['<rootDir>/src'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ['**/?(*.)(spec|test).ts'],
  moduleFileExtensions: ['ts', 'js', 'json', 'node'],
  globals: {
    'ts-jest': {
      diagnostics: false
    }
  },
  collectCoverage: true,
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 90
    }
  },
  collectCoverageFrom: [
    // '!src/common/**'
    // 'src/**/*.{ts,js}',
    // '!src/service/serve-*.{ts, js}',
    // '!src/**/*.(type|enum).{ts,js}',
    // '!src/api/*.{ts,js}',
    // '!**/__mocks__/**',
    // '!**/__tests__/**',
    // '!src/**/*.{sql.ts, sql.js}',
    // '!**/node_modules/**'
    // '!src/common/__test__/',
    // '!src/db/*'
  ]
};
