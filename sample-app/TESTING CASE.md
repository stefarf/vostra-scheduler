```text
eodhandler
    create job [
        collect(jk:j4 gk: g3, p:{sleep:4} )
    ]
    create job [
        collect(jk:j4 gk: g3, p:{sleep:4} ) // duplicate entry, ignored 
        done2(on:g3 d, p:{} )
    ]

timeticker
    create job [
        collect(gk: g1, p:{sleep:1} )
        collect(gk: g1, p:{sleep:2} )
        collect(gk: g1, p:{sleep:3} )
        
        collect(gk: g2, p:{sleep:5} )
        collect(gk: g2, p:{sleep:6} )
        
        done(gk: d, on:g1, p:{doneSleep:3} ) // 7
        done(gk: d, on:g2, p:{doneSleep:2} ) // 8
    ]

collect():
    sleep
    return { message: 'collect $sleep' }

done():
    sleep
    return { message: 'done $doneSleep' }

done2 ()
    return { message: 'all done', jobs: [
        jobId, result
    ] }    
```