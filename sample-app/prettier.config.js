module.exports = {
  trailingComma: 'none',
  printWidth: 80,
  useTabs: false,
  singleQuote: true,
  bracketSpacing: true,
  arrowParens: 'always'
};
