import dumper from '../common/dumper';
import helper from '../common/helper';
import { JobHandler } from '../common/scheduler/scheduler.type';
import { CollectPayload, CollectResult } from './collect.type';

const collectJobHandler: JobHandler<CollectPayload, CollectResult> =
  async function (body) {
    // await timeHelper.sleep(body.Payload.sleep);
    console.log(body);

    const result = { message: `collect ${body.Payload.sleep}` };
    dumper.dumpJson({ body, result }, 'collect', body.Payload.id);
    dumper.dumpJson('', 'collect', body.Payload.id, helper.shortUUID());
    return result;
  };

export default collectJobHandler;
