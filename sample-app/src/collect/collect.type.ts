import joi from 'joi';

export interface CollectPayload {
  id: string;
  sleep: number;
}
export const CollectPayloadSchema = joi.object({
  id: joi.string().required(),
  sleep: joi.number().required()
});

export interface CollectResult {
  message: string;
}
export const CollectResultSchema = joi.object({
  message: joi.string().required()
});
