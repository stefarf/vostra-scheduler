import { CreateJobRequest } from '../common/scheduler/scheduler.type';
import { JobEnum } from '../enum';
import { CollectPayload } from './collect.type';

function createJobRequest(obj: {
  JobKey?: string;
  GroupKey?: string;
  Payload: CollectPayload;
}): CreateJobRequest {
  return { JobName: JobEnum.COLLECT, ...obj };
}

const collect = { createJobRequest };
export default collect;
