export enum EndpointEnum {
  ROLE_EOD = '/role/eod',
  ROLE_TICKER = '/role/ticker',
  JOB_COLLECT = '/job/collect',
  JOB_DONE = '/job/done',
  JOB_DONE2 = '/job/done2'
}

export enum RoleEnum {
  EOD = 'EOD',
  TICKER = 'TICKER'
}

export enum JobEnum {
  COLLECT = 'COLLECT',
  DONE = 'DONE',
  DONE2 = 'DONE2'
}
