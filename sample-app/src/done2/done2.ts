import { CreateJobRequest } from '../common/scheduler/scheduler.type';
import { JobEnum } from '../enum';
import { Done2Payload } from './done2.type';

function createJobRequest(obj: {
  JobKey?: string;
  GroupKey?: string;
  Payload: Done2Payload;
  OnDoneGroups: string[];
}): CreateJobRequest {
  return { JobName: JobEnum.DONE2, ...obj };
}

const done2 = { createJobRequest };
export default done2;
