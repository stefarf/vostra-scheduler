import joi from 'joi';
import dumper from '../common/dumper';
import helper from '../common/helper';
import schedulerAPI from '../common/scheduler/scheduler.api';
import { JobHandler } from '../common/scheduler/scheduler.type';
import { Done2Payload, Done2Result } from './done2.type';

const done2JobHandler: JobHandler<Done2Payload, Done2Result> = async function (
  body
) {
  console.log(body);

  const jobs = [];
  if (body.DoneJobIds)
    for (const doneJobId of body.DoneJobIds) {
      const jobInfo = await schedulerAPI.getJobInfo<unknown>(
        joi.any(),
        doneJobId
      );
      jobs.push({ jobId: jobInfo.JobId, result: jobInfo.Result });
    }

  const result: Done2Result = { message: 'all done', jobs };
  dumper.dumpJson({ body, result }, 'done2', body.Payload.id);
  dumper.dumpJson('', 'done2', body.Payload.id, helper.shortUUID());
  return result;
};

export default done2JobHandler;
