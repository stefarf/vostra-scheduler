import joi from 'joi';

export interface Done2Payload {
  id: string;
}
export const Done2PayloadSchema = joi.object({
  id: joi.string().required()
});

export interface Done2Result {
  message: string;
  jobs: {
    jobId: number;
    result: unknown;
  }[];
}
export const Done2ResultSchema = joi.object({
  message: joi.string().required(),
  jobs: joi
    .array()
    .items(
      joi.object({
        jobId: joi.number().required(),
        result: joi.any().required()
      })
    )
    .required()
});
