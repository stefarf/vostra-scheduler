import joi from 'joi';

export interface DonePayload {
  id: string;
  doneSleep: number;
}
export const DonePayloadSchema = joi.object({
  id: joi.string().required(),
  doneSleep: joi.number().required()
});

export interface DoneResult {
  message: string;
}
export const DoneResultSchema = joi.object({
  message: joi.string().required()
});
