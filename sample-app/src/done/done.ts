import { CreateJobRequest } from '../common/scheduler/scheduler.type';
import { JobEnum } from '../enum';
import { DonePayload } from './done.type';

function createJobRequest(obj: {
  JobKey?: string;
  GroupKey?: string;
  Payload: DonePayload;
  OnDoneGroups: string[];
}): CreateJobRequest {
  return { JobName: JobEnum.DONE, ...obj };
}

const done = { createJobRequest };
export default done;
