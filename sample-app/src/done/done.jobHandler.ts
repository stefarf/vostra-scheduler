import dumper from '../common/dumper';
import helper from '../common/helper';
import { JobHandler } from '../common/scheduler/scheduler.type';
import { DonePayload, DoneResult } from './done.type';

const doneJobHandler: JobHandler<DonePayload, DoneResult> = async function (
  body
) {
  // await timeHelper.sleep(body.Payload.doneSleep);
  console.log(body);

  const result = { message: `done ${body.Payload.doneSleep}` };
  dumper.dumpJson({ body, result }, 'done', body.Payload.id);
  dumper.dumpJson('', 'done', body.Payload.id, helper.shortUUID());
  return result;
};

export default doneJobHandler;
