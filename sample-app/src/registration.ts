import collectJobHandler from './collect/collect.jobHandler';
import {
  CollectPayload,
  CollectPayloadSchema,
  CollectResult,
  CollectResultSchema
} from './collect/collect.type';
import scheduler from './common/scheduler/scheduler';
import { RegisterRequest } from './common/scheduler/scheduler.type';
import doneJobHandler from './done/done.jobHandler';
import {
  DonePayload,
  DonePayloadSchema,
  DoneResult,
  DoneResultSchema
} from './done/done.type';
import done2JobHandler from './done2/done2.jobHandler';
import {
  Done2Payload,
  Done2PayloadSchema,
  Done2Result,
  Done2ResultSchema
} from './done2/done2.type';
import { EndpointEnum, JobEnum, RoleEnum } from './enum';
import eodRoleHandler from './eod/eod.roleHandler';
import tickerRoleHandler from './ticker/ticker.roleHandler';

const registration: RegisterRequest = {
  Roles: [
    {
      Endpoint: EndpointEnum.ROLE_EOD,
      Role: RoleEnum.EOD,
      Handler: scheduler.wrapRoleHandler(eodRoleHandler)
    },
    {
      Endpoint: EndpointEnum.ROLE_TICKER,
      Role: RoleEnum.TICKER,
      Handler: scheduler.wrapRoleHandler(tickerRoleHandler)
    }
  ],
  Jobs: [
    {
      Endpoint: EndpointEnum.JOB_COLLECT,
      Job: JobEnum.COLLECT,
      Handler: scheduler.wrapJobHandler<CollectPayload, CollectResult>(
        CollectPayloadSchema,
        CollectResultSchema,
        collectJobHandler
      )
    },
    {
      Endpoint: EndpointEnum.JOB_DONE,
      Job: JobEnum.DONE,
      Handler: scheduler.wrapJobHandler<DonePayload, DoneResult>(
        DonePayloadSchema,
        DoneResultSchema,
        doneJobHandler
      )
    },
    {
      Endpoint: EndpointEnum.JOB_DONE2,
      Job: JobEnum.DONE2,
      Handler: scheduler.wrapJobHandler<Done2Payload, Done2Result>(
        Done2PayloadSchema,
        Done2ResultSchema,
        done2JobHandler
      )
    }
  ]
};

export default registration;
