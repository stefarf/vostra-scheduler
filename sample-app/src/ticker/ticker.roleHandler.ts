import collect from '../collect/collect';
import schedulerAPI from '../common/scheduler/scheduler.api';
import { RoleHandler } from '../common/scheduler/scheduler.type';
import done from '../done/done';

const tickerRoleHandler: RoleHandler = async function (_role) {
  await schedulerAPI.createJob([
    collect.createJobRequest({
      GroupKey: 'g1',
      Payload: { id: '[1]', sleep: 1 }
    }),
    collect.createJobRequest({
      GroupKey: 'g1',
      Payload: { id: '[2]', sleep: 2 }
    }),
    collect.createJobRequest({
      GroupKey: 'g1',
      Payload: { id: '[3]', sleep: 3 }
    }),

    collect.createJobRequest({
      GroupKey: 'g2',
      Payload: { id: '[5]', sleep: 5 }
    }),
    collect.createJobRequest({
      GroupKey: 'g2',
      Payload: { id: '[6]', sleep: 6 }
    }),

    done.createJobRequest({
      GroupKey: 'd',
      OnDoneGroups: ['g1'],
      Payload: { id: '[7]', doneSleep: 4 }
    }),
    done.createJobRequest({
      GroupKey: 'd',
      OnDoneGroups: ['g2'],
      Payload: { id: '[8]', doneSleep: 3 }
    })
  ]);

  return { sleepSecs: 3600 };
};

export default tickerRoleHandler;
