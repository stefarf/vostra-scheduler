function mustGetEnv(name: string): string {
  const val = process.env[name];
  if (val === undefined) throw new Error(`Require ${name} environment`);
  return val;
}

const env = {
  mustGetEnv,
  MYSQL_PASSWORD: mustGetEnv('MYSQL_PASSWORD')
};
export default env;
