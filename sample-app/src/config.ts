const config = {
  mysql: {
    host: 'localhost',
    port: 3306,
    user: 'root',
    database: 'dev'
  }
};
export default config;
