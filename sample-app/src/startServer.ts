import express, { Express } from 'express';
import schedulerEnv from './common/scheduler/scheduler.env';

const app = express();
app.use(express.json());

async function startServer(
  initEndpoint: (app: Express) => void,
  serverReady: () => Promise<void>
) {
  initEndpoint(app);
  app.listen(schedulerEnv.APP_PORT, async () => {
    console.log(`App server is running at port ${schedulerEnv.APP_PORT}`);
    await serverReady();
  });
}

export default startServer;
