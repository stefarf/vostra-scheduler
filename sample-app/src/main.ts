import schedulerAPI from './common/scheduler/scheduler.api';
import registration from './registration';
import startServer from './startServer';

async function main() {
  await schedulerAPI.register(registration);

  await startServer(
    (app) => {
      if (registration.Roles)
        registration.Roles.forEach((reg) => {
          app.post(reg.Endpoint, reg.Handler);
        });
      if (registration.Jobs)
        registration.Jobs.forEach((reg) => {
          app.post(reg.Endpoint, reg.Handler);
        });
    },
    async () => {
      console.log('server ready');
    }
  );
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
