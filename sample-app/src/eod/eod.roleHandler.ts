import collect from '../collect/collect';
import schedulerAPI from '../common/scheduler/scheduler.api';
import { RoleHandler } from '../common/scheduler/scheduler.type';
import done2 from '../done2/done2';

const eodRoleHandler: RoleHandler = async function (_role) {
  await schedulerAPI.createJob([
    collect.createJobRequest({
      JobKey: 'j4',
      GroupKey: 'g3',
      Payload: { id: '[4]', sleep: 4 }
    })
  ]);

  await schedulerAPI.createJob([
    collect.createJobRequest({
      JobKey: 'j4',
      GroupKey: 'g3',
      Payload: { id: '[?]', sleep: 4 }
    }),
    done2.createJobRequest({
      JobKey: 'j9',
      OnDoneGroups: ['g3', 'd'],
      Payload: { id: '[9]' }
    })
  ]);

  return { sleepSecs: 5 };
};

export default eodRoleHandler;
