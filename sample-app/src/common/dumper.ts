import * as fs from 'fs';
import path from 'path';
import helper from './helper';
import momentHelper from './moment.helper';

function getFullPath(ext: string, ...paths: string[]): string {
  return (
    path
      .join(__dirname, '../../dump', ...paths)
      .replace(/[^a-zA-Z\d/\-_. :]/g, '_') + ext
  );
}

function dumpString(s: string, ...paths: string[]) {
  const fullPath = getFullPath('.txt', ...paths);
  // console.log('DUMP:', fullPath);
  fs.mkdirSync(path.dirname(fullPath), { recursive: true });
  fs.writeFileSync(fullPath, s, 'utf8');
}

function dumpJson(data: unknown, ...paths: string[]) {
  const fullPath = getFullPath('.json', ...paths);
  // console.log('DUMP:', fullPath);
  fs.mkdirSync(path.dirname(fullPath), { recursive: true });
  fs.writeFileSync(fullPath, JSON.stringify(data, null, 2), 'utf8');
}

function catchErr(err: unknown) {
  console.error(err);
  dumpString(
    helper.utilInspect(err),
    'error',
    'dumperCatchErr',
    `${momentHelper.now()} ${helper.shortUUID()}`
  );
}

function catchErrAndExit(err: unknown) {
  catchErr(err);
  process.exit(1);
}

const dumper = { dumpString, dumpJson, catchErr, catchErrAndExit };
export default dumper;
