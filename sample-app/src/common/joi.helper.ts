import joi from 'joi';

function validateAsType<ValueType>(
  schema: joi.AnySchema,
  v: unknown
): ValueType {
  const rst = schema.validate(v);
  if (rst.error) throw new Error(rst.error.toString());
  return v as ValueType;
}

const joiHelper = { validateAsType };
export default joiHelper;
