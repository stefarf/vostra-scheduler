import moment from 'moment-timezone';

const JKT = 'Asia/Jakarta';
const FORMAT_DATETIME_GMT7 = 'YYYY-MM-DDTHH:mm:ss+07:00';
const FORMAT_DATETIME = 'YYYY-MM-DD HH:mm:ss';
const FORMAT_DATE = 'YYYY-MM-DD';

function timezone(dt: string) {
  return moment(dt).tz(JKT);
}

function now(): string {
  return moment().tz(JKT).format(FORMAT_DATETIME);
}

function today(): string {
  return moment().tz(JKT).format(FORMAT_DATE);
}

function yesterday(): string {
  return moment().tz(JKT).subtract(1, 'day').format(FORMAT_DATE);
}

function daysAfter(days: number, date: string): string {
  return moment(date).tz(JKT).add(days, 'days').format(FORMAT_DATE);
}

function daysBefore(days: number, date: string): string {
  return moment(date).tz(JKT).subtract(days, 'days').format(FORMAT_DATE);
}

function addSeconds(dt: string, secs: number) {
  return moment(dt).add(secs, 'second').format(FORMAT_DATETIME);
}

const momentHelper = {
  FORMAT_DATETIME_GMT7,
  FORMAT_DATETIME,
  FORMAT_DATE,
  timezone,
  now,
  today,
  yesterday,
  daysAfter,
  daysBefore,
  addSeconds
};
export default momentHelper;
