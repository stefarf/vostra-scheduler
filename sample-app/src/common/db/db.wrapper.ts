import mysql from 'mysql';
import env from '../../env';
import dbLib from './db.lib';
import { DBTransaction } from './db.transaction';
import { DBConfig, DBQueryResult } from './db.type';

export class DBWrapper {
  private conn: any = null;

  connect(config: DBConfig) {
    if (this.conn) this.conn.end();
    this.conn = mysql.createConnection({
      ...config,
      password: env.MYSQL_PASSWORD,
      multipleStatements: true,
      supportBigNumbers: true,
      bigNumberStrings: true
    });
    this.conn.connect();
  }

  async query(q: string, args?: any[]): Promise<DBQueryResult> {
    return await dbLib.query(this.conn, q, args); // return { rows, fields }
  }

  async transaction(cb: (tx: DBTransaction) => Promise<void>): Promise<void> {
    await dbLib.beginTransaction(this.conn);
    const tx = new DBTransaction(this.conn);
    try {
      await cb(tx);
      await tx.commit();
    } catch (err) {
      await tx.rollback();
      throw err;
    }
  }

  async queryStream(
    q: string,
    args: any[] | undefined,
    cb: (row: any) => Promise<void>
  ): Promise<void> {
    return await dbLib.queryStream(this.conn, q, args, cb);
  }

  async end(): Promise<void> {
    this.conn.end();
    this.conn = null;
  }
}
