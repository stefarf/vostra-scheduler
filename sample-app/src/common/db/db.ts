import config from '../../config';
import { DBWrapper } from './db.wrapper';

const db = new DBWrapper();
db.connect(config.mysql);

export default db;
