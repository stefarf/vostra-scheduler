export interface DBQueryResult {
  rows: any[];
  fields: any;
}

export interface DBConfig {
  host: string;
  port: number;
  user: string;
  database: string;
}
