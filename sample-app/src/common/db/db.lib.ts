import { DBQueryResult } from './db.type';

function query(conn: any, q: string, args?: any[]): Promise<DBQueryResult> {
  return new Promise((resolve, reject) => {
    conn.query(q, args, (err: any, rows: any[], fields: any) => {
      if (err) {
        reject(err);
        return;
      }
      resolve({ rows, fields });
    });
  });
}

function queryStream(
  conn: any,
  q: string,
  args: any[] | undefined,
  cb: (row: any) => Promise<void>
): Promise<void> {
  return new Promise((resolve, reject) => {
    conn
      .query(q, args)
      .on('error', function (err: any) {
        reject(err);
      })
      .on('result', function (row: any) {
        conn.pause();
        cb(row).then(conn.resume).catch(reject);
      })
      .on('end', function () {
        resolve();
      });
  });
}

function beginTransaction(conn: any): Promise<void> {
  return new Promise((resolve, reject) => {
    conn.beginTransaction((err: any) => {
      if (err) {
        reject(err);
        return;
      }
      resolve();
    });
  });
}

function commit(conn: any): Promise<void> {
  return new Promise((resolve, reject) => {
    conn.commit((err: any) => {
      if (err) {
        reject(err);
        return;
      }
      resolve();
    });
  });
}

function rollback(conn: any): Promise<void> {
  return new Promise((resolve) => {
    conn.rollback(() => {
      resolve();
    });
  });
}

const dbLib = {
  query,
  queryStream,
  beginTransaction,
  commit,
  rollback
};
export default dbLib;
