import dbLib from './db.lib';
import { DBQueryResult } from './db.type';

export class DBTransaction {
  readonly conn: any;
  private ended = false;

  constructor(conn: any) {
    this.conn = conn;
  }

  async query(q: string, args: any[]): Promise<DBQueryResult> {
    if (this.ended)
      throw new Error('Can not query in ended/closed transaction');
    return await dbLib.query(this.conn, q, args);
  }

  async queryStream(
    q: string,
    args: any[] | undefined,
    cb: (row: any) => Promise<void>
  ): Promise<void> {
    if (this.ended)
      throw new Error('Can not query in ended/closed transaction');
    return await dbLib.queryStream(this.conn, q, args, cb);
  }

  async commit(): Promise<void> {
    if (this.ended) return;
    this.ended = true;
    await dbLib.commit(this.conn);
  }

  async rollback(): Promise<void> {
    if (this.ended) return;
    this.ended = true;
    await dbLib.rollback(this.conn);
  }
}
