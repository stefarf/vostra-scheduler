import joi from 'joi';

export interface AxiosError {
  response: {
    status: number;
  };
}

export const AxiosErrorSchema = joi
  .object({
    response: joi
      .object({
        status: joi.number().required()
      })
      .options({ allowUnknown: true })
      .required()
  })
  .options({ allowUnknown: true });
