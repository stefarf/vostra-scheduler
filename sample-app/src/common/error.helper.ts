import { AxiosError, AxiosErrorSchema } from './error.type';
import joiHelper from './joi.helper';

function toString(err: unknown): string {
  const e = err as any;
  if (e.toString) return e.toString();
  return 'UNKNOWN ERROR';
}

function toAxiosError(err: unknown) {
  return joiHelper.validateAsType<AxiosError>(AxiosErrorSchema, err);
}

const errorHelper = { toString, toAxiosError };
export default errorHelper;
