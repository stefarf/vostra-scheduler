import env from '../../env';

const schedulerEnv = {
  INSTANCE_ID: env.mustGetEnv('INSTANCE_ID'),
  SCHEDULER_PORT: Number(env.mustGetEnv('SCHEDULER_PORT')),
  APP_PORT: Number(env.mustGetEnv('APP_PORT'))
};
export default schedulerEnv;
