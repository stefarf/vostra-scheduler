import axios from 'axios';
import joi from 'joi';
import joiHelper from '../joi.helper';
import schedulerEnv from './scheduler.env';
import {
  CreateJobRequest,
  JobResult,
  JobResultSchema,
  RegisterRequest
} from './scheduler.type';

const api = axios.create({
  baseURL: `http://localhost:${schedulerEnv.SCHEDULER_PORT}`,
  timeout: 1000,
  headers: { 'Instance-Id': schedulerEnv.INSTANCE_ID }
});

async function register(request: RegisterRequest) {
  await api.post('/register', request);
}

async function createJob(request: CreateJobRequest[]) {
  await api.post('/job', request);
}

async function getJobInfo<ResultType>(
  ResultSchema: joi.AnySchema,
  jobId: number
) {
  const res = await api.get(`/job/${jobId}`);
  const jobResult = joiHelper.validateAsType<JobResult<ResultType>>(
    JobResultSchema,
    res.data
  );
  joiHelper.validateAsType<ResultType>(ResultSchema, jobResult.Result);
  return jobResult;
}

const schedulerAPI = { register, createJob, getJobInfo };
export default schedulerAPI;
