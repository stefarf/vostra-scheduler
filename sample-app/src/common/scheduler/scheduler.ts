import { RequestHandler } from 'express';
import joi from 'joi';
import joiHelper from '../joi.helper';
import timeHelper from '../time.helper';
import {
  JobPayload,
  JobPayloadSchema,
  JobHandler,
  RoleHandler
} from './scheduler.type';

function wrapRoleHandler(handler: RoleHandler): RequestHandler {
  return (req, res) => {
    const run = async () => {
      const role = req.header('X-Role') || '';
      const { sleepSecs } = await handler(role);
      await timeHelper.sleep(sleepSecs);
      res.status(200).end();
    };
    run().catch((err) => {
      console.error(err);
      res.status(500).end();
    });
  };
}

function wrapJobHandler<PayloadType, ResultType>(
  PayloadSchema: joi.AnySchema,
  ResultSchema: joi.AnySchema,
  handler: JobHandler<PayloadType, ResultType>
): RequestHandler {
  return (req, res) => {
    const run = async () => {
      const body = joiHelper.validateAsType<JobPayload<PayloadType>>(
        JobPayloadSchema,
        req.body
      );
      // The JobBodySchema assume payload as unknown, so need additional validation
      joiHelper.validateAsType<PayloadType>(PayloadSchema, body.Payload);

      const result = joiHelper.validateAsType<ResultType>(
        ResultSchema,
        await handler(body)
      );
      res.json(result);
    };
    run().catch((err) => {
      console.error(err);
      res.status(500).end();
    });
  };
}

const scheduler = { wrapRoleHandler, wrapJobHandler };
export default scheduler;
