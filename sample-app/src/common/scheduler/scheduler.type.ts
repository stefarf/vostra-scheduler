import { RequestHandler } from 'express';
import joi from 'joi';

export interface JobPayload<PayloadType> {
  JobId: number;
  JobName: string;
  JobKey: string | null;
  GroupKey: string | null;
  DoneJobIds: number[] | null;
  Payload: PayloadType;
}
export const JobPayloadSchema = joi.object({
  JobId: joi.number().required(),
  JobName: joi.string().required(),
  JobKey: joi.string().allow(null).required(),
  GroupKey: joi.string().allow(null).required(),
  DoneJobIds: joi.array().items(joi.number()).allow(null).required(),
  Payload: joi.any().required()
});

export interface JobResult<ResultType> {
  JobId: number;
  JobName: string;
  JobKey: string | null;
  GroupKey: string | null;
  DoneJobIds: number[] | null;
  Result: ResultType;
}
export const JobResultSchema = joi.object({
  JobId: joi.number().required(),
  JobName: joi.string().required(),
  JobKey: joi.string().allow(null).required(),
  GroupKey: joi.string().allow(null).required(),
  DoneJobIds: joi.array().items(joi.number()).allow(null).required(),
  Result: joi.any().required()
});

export type RoleHandler = (role: string) => Promise<{ sleepSecs: number }>;

export type JobHandler<PayloadType, ResultType> = (
  body: JobPayload<PayloadType>
) => Promise<ResultType>;

export interface RegisterRequest {
  Roles?: {
    Endpoint: string;
    Role: string;
    Handler: RequestHandler;
  }[];
  Jobs?: {
    Endpoint: string;
    Job: string;
    Handler: RequestHandler;
  }[];
}

export interface CreateJobRequest {
  JobName: string;
  JobKey?: string;
  GroupKey?: string;
  Payload: unknown;
  OnDoneGroups?: string[];
}
