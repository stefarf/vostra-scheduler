import util from 'util';
import { v4 as uuidv4 } from 'uuid';

function utilInspect(v: unknown, depth: number = 5): string {
  return util.inspect(v, false, depth);
}

function shortUUID(): string {
  return uuidv4().split('-').at(-1) || '';
}

const helper = { uuidv4, shortUUID, utilInspect };
export default helper;
