import momentHelper from './common/moment.helper';

interface ManualAndTime {
  manual: boolean;
  now: string;
  today: string;
  reportDate: string;
}

function manualAndTime(): ManualAndTime {
  let reportDate = process.env.REPORT_DATE;
  if (reportDate) {
    // manual
    reportDate = momentHelper
      .timezone(reportDate)
      .format(momentHelper.FORMAT_DATE);

    const manual = process.env.MANUAL !== 'non-manual';
    const today = momentHelper.daysAfter(1, reportDate);
    const now = `${today} 23:59:59`;
    return { manual, now, today, reportDate };
  } else {
    // real time
    return {
      manual: false,
      now: momentHelper.now(),
      today: momentHelper.today(),
      reportDate: momentHelper.yesterday()
    };
  }
}

export default manualAndTime;
