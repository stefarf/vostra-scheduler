#!/usr/bin/env bash
fnm use 16.14.0
npm i \
  axios \
  express \
  joi \
  moment-timezone \
  mysql \
  mysqldump \
  uuid
npm i -D \
  @types/express \
  @types/jest \
  @types/moment-timezone \
  @types/mysql \
  @types/node \
  @types/uuid \
  @typescript-eslint/eslint-plugin \
  @typescript-eslint/parser \
  eslint \
  eslint-config-standard \
  eslint-config-prettier \
  eslint-plugin-import \
  eslint-plugin-jest \
  eslint-plugin-node \
  eslint-plugin-prettier \
  eslint-plugin-promise \
  jest \
  prettier \
  ts-jest \
  ts-node \
  typescript
