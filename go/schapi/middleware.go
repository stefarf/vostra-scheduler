package schapi

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

func mdwRecover(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		defer func() {
			if e := recover(); e != nil {
				switch v := e.(type) {
				case error:
					err = c.String(http.StatusInternalServerError, v.Error())
				case fmt.Stringer:
					err = c.String(http.StatusInternalServerError, v.String())
				default:
					err = c.String(http.StatusInternalServerError, fmt.Sprintf("%v", e))
				}
			}
		}()
		return next(c)
	}
}
