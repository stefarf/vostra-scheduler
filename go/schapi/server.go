package schapi

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"

	"vostra/go/iferr"
	"vostra/go/ref"
	"vostra/go/scheduler"
)

func RunServer(port int, m map[string]*scheduler.Scheduler) {
	e := echo.New()
	e.HideBanner = true

	e.Use(mdwRecover)

	e.POST("/register", register(m))
	e.POST("/job", createJob(m))
	e.GET("/job/:jobId", getJobInfo(m))

	iferr.Fatal(e.Start(fmt.Sprintf(":%d", port)))
}

func register(m map[string]*scheduler.Scheduler) echo.HandlerFunc {
	return func(c echo.Context) error {
		instanceId := c.Request().Header.Get("Instance-Id")
		sch, ok := m[instanceId]
		if !ok {
			return c.String(http.StatusNotFound, "unknown instance id")
		}

		var body ref.RegisterRequest
		iferr.Panic(c.Bind(&body))
		sch.Register(body)
		return c.String(http.StatusOK, "registered")
	}
}

func createJob(m map[string]*scheduler.Scheduler) echo.HandlerFunc {
	return func(c echo.Context) error {
		instanceId := c.Request().Header.Get("Instance-Id")
		sch, ok := m[instanceId]
		if !ok {
			return c.String(http.StatusNotFound, "unknown instance id")
		}

		var body []ref.CreateJobRequest
		iferr.Panic(c.Bind(&body))
		sch.CreateJobs(body)
		return c.String(http.StatusOK, "job created")
	}
}

func getJobInfo(m map[string]*scheduler.Scheduler) echo.HandlerFunc {
	return func(c echo.Context) error {
		instanceId := c.Request().Header.Get("Instance-Id")
		sch, ok := m[instanceId]
		if !ok {
			return c.String(http.StatusNotFound, "unknown instance id")
		}

		jobId, err := strconv.ParseInt(c.Param("jobId"), 10, 64)
		if err != nil {
			return err
		}
		jobInfo := sch.GetJobInfo(uint64(jobId))
		return c.JSONBlob(http.StatusOK, jobInfo)
	}
}
