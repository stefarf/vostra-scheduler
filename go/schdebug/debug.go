package schdebug

import "log"

type debug bool

func (d debug) Println(v ...any) {
	if d {
		log.Println(v...)
	}
}

func (d debug) Printf(format string, v ...any) {
	if d {
		log.Printf(format, v...)
	}
}

var (
	InstanceState debug = false
	RunJob        debug = true
	AssignJobs    debug = false

	DBJobs   debug = false
	DBEvents debug = false
	DBOnDone debug = false
)
