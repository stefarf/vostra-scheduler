package ref

import (
	"encoding/json"
)

type (
	RegisterRequest struct {
		Roles []RegisterRoleRequest
		Jobs  []RegisterJobRequest
	}

	RegisterRoleRequest struct {
		Endpoint string
		Role     string
	}

	RegisterJobRequest struct {
		Endpoint string
		Job      string
	}

	CreateJobRequest struct {
		JobName      string
		JobKey       string
		GroupKey     string
		Payload      json.RawMessage
		OnDoneGroups []string
	}
)
