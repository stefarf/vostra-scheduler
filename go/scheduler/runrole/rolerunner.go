package runrole

import (
	"sync"
	"time"

	"vostra/go/common"
	"vostra/go/ref"
)

type (
	RunRole struct {
		appPort int
		apiCall func(instanceId, endpoint, role string, appPort int)

		running bool
		askStop bool
		wg      sync.WaitGroup
	}

	Runner interface {
		Start(instanceId string, assignments []ref.RegisterRoleRequest)
		Stop()
		IsRunning() bool
	}
)

var _ Runner = &RunRole{}

func New(appPort int, apiCall func(instanceId, endpoint, role string, appPort int)) *RunRole {
	return &RunRole{appPort: appPort, apiCall: apiCall}
}

// Start starts all routines to call each endpoint listed in the `endpoints`
func (r *RunRole) Start(instanceId string, assignments []ref.RegisterRoleRequest) {
	if r.running {
		return
	}
	r.running = true
	r.askStop = false

	r.wg = sync.WaitGroup{}
	for _, i := range assignments {
		r.wg.Add(1)
		go func(endpoint, role string) {
			defer r.wg.Done()
			for {
				r.runAndRecover(instanceId, endpoint, role)

				// if runAndRecover panics, sleep for a while before retrying
				wake := time.Now().Add(time.Second * 10)
				for time.Now().Before(wake) {
					if r.askStop {
						return
					}
					time.Sleep(time.Millisecond * 100)
				}
			}
		}(i.Endpoint, i.Role)
	}
}

func (r *RunRole) runAndRecover(instanceId, endpoint, role string) {
	defer common.RecoverAndLog()
	for {
		if r.askStop {
			return
		}
		r.apiCall(instanceId, endpoint, role, r.appPort)
		wake := time.Now().Add(time.Second)
		for time.Now().Before(wake) {
			if r.askStop {
				return
			}
			time.Sleep(time.Millisecond * 100)
		}
	}
}

// Stop will stop all currently running role handlers and wait until they are all stopped
func (r *RunRole) Stop() {
	if !r.running {
		return
	}
	r.askStop = true
	r.wg.Wait()
	r.running = false
	r.askStop = false
}

func (r *RunRole) IsRunning() bool { return r.running }
