package runrole

import (
	"testing"
	"time"

	"vostra/go/ref"
)

func TestRunner_StartStop(t *testing.T) {
	// Setup
	count := 0
	apiCall := func(instanceId, endpoint, role string, appPort int) {
		count++
		if endpoint != "/endpoint" {
			t.Error("incorrect endpoint")
		}
		if role != "role" {
			t.Error("incorrect role")
		}
		if appPort != 0 {
			t.Error("incorrect appPort")
		}
		time.Sleep(time.Millisecond * 10)
	}
	r := New(0, apiCall)
	assignments := []ref.RegisterRoleRequest{
		{Endpoint: "/endpoint", Role: "role"},
	}

	// Testing
	r.Start("", assignments)
	if !r.running {
		t.Error("incorrect running")
	}
	time.Sleep(50 * time.Millisecond)
	if count != 1 {
		t.Error("must be executed 1 time")
	}
	r.Stop()
	if r.running {
		t.Error("incorrect running")
	}

	count = 0
	r.Start("", assignments)
	time.Sleep(1100 * time.Millisecond)
	if count != 2 {
		t.Error("must be executed 2 times")
	}
	r.Stop()
}
