package scheduler

import (
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/jmoiron/sqlx"

	"vostra/go/common"
	"vostra/go/dbhelper"
	"vostra/go/ref"
	"vostra/go/scheduler/runapp"
	"vostra/go/scheduler/runrole"
)

// DummyAppRunner

type DummyAppRunner struct {
	appPort      int
	sch          *Scheduler
	registration ref.RegisterRequest
	running      bool
}

func (d *DummyAppRunner) Start(instanceId string) {
	if d.running {
		return
	}
	d.running = true
	d.sch.Register(d.registration)
}
func (d *DummyAppRunner) Stop() error {
	d.running = false
	return nil
}
func (d *DummyAppRunner) Port() int {
	return d.appPort
}

// Test

func TestScheduler(t *testing.T) {
	_ = os.Setenv("MYSQL_PASSWORD", "secret")
	db := dbhelper.ConnectAndInit("localhost", 3306, "scheduler", "root", "MYSQL_PASSWORD")
	CreateTables(db, true)

	testDurationUnit := time.Millisecond * 50
	initDuration(testDurationUnit)

	m := map[string]*Scheduler{}

	roleApiCall := func(instanceId, endpoint, role string, appPort int) {
		defer common.RecoverAndLog()

		today := time.Now().Format("2006-01-02")
		sch := m[instanceId]
		switch role {
		case "r1":
			if endpoint != "/r1" {
				t.Errorf("expect endpoint /r1, got %s", endpoint)
			}
			sch.CreateJobs([]ref.CreateJobRequest{
				{JobName: "j1", JobKey: fmt.Sprintf("[1]%s", today), GroupKey: "g1"},
				{JobName: "j1", JobKey: fmt.Sprintf("[2]%s", today), GroupKey: "g1"},
				{JobName: "j1", JobKey: fmt.Sprintf("[3]%s", today), GroupKey: "g1"},
				{JobName: "j12", JobKey: fmt.Sprintf("[6]%s", today), OnDoneGroups: []string{"g1", "g2"}},
			})

		case "r2":
			if endpoint != "/r2" {
				t.Errorf("expect endpoint /r2, got %s", endpoint)
			}
			sch.CreateJobs([]ref.CreateJobRequest{
				{JobName: "j2", JobKey: fmt.Sprintf("[4]%s", today), GroupKey: "g2"},
				{JobName: "j2", JobKey: fmt.Sprintf("[5]%s", today), GroupKey: "g2"},
				{JobName: "j12", JobKey: fmt.Sprintf("[7]%s", today), OnDoneGroups: []string{"g1", "g2"}},
			})

		default:
			t.Errorf("invalid role %s", role)
		}

	}
	defaultNewRunRole = func(
		appPort int,
		apiCall func(instanceId, endpoint string, role string, appPort int),
	) runrole.Runner {
		return runrole.New(0, roleApiCall)
	}

	chJobsDone := make(chan bool)
	cntJob := 0
	defaultCallJobHandler = func(instanceId, endpoint string, appPort int, jobInfo []byte) (result []byte) {
		defer func() {
			cntJob++
			if cntJob == 7 {
				chJobsDone <- true
			}
		}()
		return jobInfo
	}

	registration := ref.RegisterRequest{
		Roles: []ref.RegisterRoleRequest{
			{Endpoint: "/r1", Role: "r1"},
			{Endpoint: "/r2", Role: "r2"},
		},
		Jobs: []ref.RegisterJobRequest{
			{Endpoint: "/j1", Job: "j1"},
			{Endpoint: "/j2", Job: "j2"},
			{Endpoint: "/j12", Job: "j12"},
		},
	}

	var apps []*DummyAppRunner
	createAppRunner := func(appPort int) runapp.Runner {
		app := &DummyAppRunner{appPort: appPort, registration: registration}
		apps = append(apps, app)
		return app
	}
	schedulerCreated := func(instanceId string, sch *Scheduler) {
		m[instanceId] = sch
		app := apps[0]
		apps = apps[1:]
		app.sch = sch
	}
	opt := Options{
		NumberOfInstances:     2,
		AppPortStartFrom:      100,
		DurationPreparation:   durationUnit * 10,
		DurationFullJobQ:      durationUnit * 5,
		DurationLessJobQ:      durationUnit,
		DurationNoJobAssigned: durationUnit,
		DurationNoMoreEvent:   durationUnit,
		JobsMaxQueue:          20,
		JobsRemaining:         20,
	}
	StartScheduler(opt, db, createAppRunner, schedulerCreated)
	opt.AppPortStartFrom = 110
	StartScheduler(opt, db, createAppRunner, schedulerCreated)

	<-chJobsDone
	time.Sleep(durationUnit * 2)

	lastId := testLogsMessages(
		t, db, 0,
		func(id uint64, message string) bool {
			if message == "register self to workers map" {
				return true
			}
			return false
		},
		[]string{
			"propose self as leader",
			"leader elected and next state: preparation",
			"done process registration",
			"confirm stop",
			"confirm stop",
			"confirm stop",
			"confirm all workers to stop and next state: assignment",
			"assign and next state: running",
		})

	_ = lastId

	//_ = testLogsMessages(
	//	t, db, lastId,
	//	func(id uint64, message string) bool {
	//		return false
	//	},
	//	[]string{
	//		"updated registrations, next state: preparation",
	//		"confirm all workers to stop and next state: assignment",
	//		"assign and next state: running",
	//	})

}

func testLogsMessages(
	t *testing.T,
	db *sqlx.DB,
	lastId uint64,
	skip func(id uint64, message string) bool,
	want []string,
) uint64 {
	var got []string

	rows, err := db.Query(
		"SELECT id, message FROM vs_logs WHERE id > ? ORDER BY id", lastId)
	if err != nil {
		t.Error(err)
	}
	for rows.Next() {
		var id uint64
		var message string
		err := rows.Scan(&id, &message)
		if err != nil {
			t.Error(err)
		}
		lastId = id

		if id == 1 && message != "propose self as leader" {
			t.Error("id 1 must be 'propose self as leader'")
		}
		if skip(id, message) {
			continue
		}
		got = append(got, message)
	}

	gotStr := strings.Join(got, "\n")
	wantStr := strings.Join(want, "\n")
	if gotStr != wantStr {
		t.Errorf("got:\n\n%s\n\nwant:\n\n%s\n\n", gotStr, wantStr)
	}

	return lastId
}
