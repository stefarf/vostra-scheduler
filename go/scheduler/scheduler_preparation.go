package scheduler

import (
	"encoding/json"
	"sync"
	"time"

	"vostra/go/iferr"
	"vostra/go/ref"
)

func (sch *Scheduler) leaderInPreparationState(nextId uint64) {
	now := time.Now()

	// Stop assign jobs and process events
	if sch.lAssignJobs.IsRunning() || sch.lProcessEvents.IsRunning() {
		var wg sync.WaitGroup
		wg.Add(2)
		go func() {
			defer wg.Done()
			sch.lAssignJobs.Stop()
		}()
		go func() {
			defer wg.Done()
			sch.lProcessEvents.Stop()
		}()
		wg.Wait()
		// Because the stopping processes above may take long time to complete,
		// to make sure the state is still up-to-date, need to return.
		return
	}

	if now.Before(sch.slog.PrepWaitUntil) {
		// Give time for all workers to update
		sch.sleep(durationUnit)
		return
	}

	prevRegId := sch.slog.LastRegId
	sch.slog.LastRegId = qRegistrationsSelectAndCallback(sch.db, sch.slog.LastRegId, sch.leaderProcessRegistration)
	if prevRegId != sch.slog.LastRegId {
		sch.updateNextState(nextId, "done process registration")
		return
	}

	sch.instanceStateLogf("registered workers: %d", len(sch.slog.WorkersMap))
	for wi, wd := range sch.slog.WorkersMap {
		if !wd.ConfirmStop {
			sch.instanceStateLogf("worker instance '%s' is still running", wi)
			sch.sleep(durationUnit)
			return
		}
	}

	sch.slog.State = stateAssignment
	sch.updateNextState(nextId, "confirm all workers to stop and next state: assignment")
}

func (sch *Scheduler) leaderProcessRegistration(reg Registration) {
	var req ref.RegisterRequest
	iferr.Panic(json.Unmarshal(reg.Payload, &req))
	sch.slog.WorkersMap[reg.InstanceId].Registration = &req
}

func (sch *Scheduler) workerInPreparationState(nextId uint64) {
	// Stop run role and run job
	if sch.wRunRole.IsRunning() || sch.wRunJob.IsRunning() {
		var wg sync.WaitGroup
		wg.Add(2)
		go func() {
			defer wg.Done()
			sch.wRunRole.Stop()
		}()
		go func() {
			defer wg.Done()
			sch.wRunJob.Stop()
		}()
		wg.Wait()
		// Because the stopping processes above may take long time to complete,
		// to make sure the state is still up-to-date, need to return.
		return
	}

	// Wait for the registration
	if sch.slog.WorkersMap[sch.InstanceId].Registration == nil {
		sch.instanceStateLogln("Empty registration")
		sch.sleep(durationUnit)
		return
	}

	// Confirm stop and update state
	workerDetail := sch.slog.WorkersMap[sch.InstanceId]
	if !workerDetail.ConfirmStop {
		workerDetail.ConfirmStop = true
		sch.updateNextState(nextId, "confirm stop")
	}
	sch.sleep(durationUnit)
}
