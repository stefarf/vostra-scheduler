package scheduler

import (
	"crypto/sha256"
	"encoding/hex"
	"log"
	"net"
	"sort"

	"vostra/go/common"
	"vostra/go/iferr"
)

type AddrsPort struct {
	Addrs []string
	Port  int
}

func getAllNetworkAddresses() (addrs []string) {
	is, err := net.Interfaces()
	iferr.Panic(err)
	for _, i := range is {
		as, err := i.Addrs()
		iferr.Panic(err)
		for _, a := range as {
			addrs = append(addrs, a.String())
		}
	}
	sort.Slice(addrs, func(i, j int) bool {
		return addrs[i] < addrs[j]
	})
	return
}

func createInstanceId(port int) string {
	ap := AddrsPort{
		Addrs: getAllNetworkAddresses(),
		Port:  port,
	}
	b := common.JsonMustMarshal(ap)
	sum := sha256.Sum256(b)
	id := hex.EncodeToString(sum[:])[:16]
	log.Printf("Port: %d, instance id: %s\n", port, id)
	return id
}
