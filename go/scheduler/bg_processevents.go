package scheduler

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"

	"vostra/go/iferr"
	"vostra/go/scheduler/runbg"
)

type ProcessEvents struct {
	db  *sqlx.DB
	bg  *runbg.RunBg
	opt Options
}

func newProcessEvents(db *sqlx.DB, opt Options) *ProcessEvents {
	return &ProcessEvents{db: db, bg: runbg.New(durationUnit / 3), opt: opt}
}

func (p *ProcessEvents) Start() {
	p.bg.Start(func() time.Duration {
		event := qEventsGetOneUndoneEvent(p.db)
		if event == nil {
			return p.opt.DurationNoMoreEvent
		}

		switch event.Name {

		case eventOnDoneJobCreated:
			p.processOnDoneJobCreatedEvent(*event)

		case eventJobDone:
			p.processJobDoneEvent(*event)

		default:
			panic(fmt.Sprintf("unknown event: '%s'", event.Name))
		}
		qEventsUpdateStatusDone(p.db, event.Id)
		return 0
	})
}

func (p *ProcessEvents) processOnDoneJobCreatedEvent(event Event) {
	var payload EventOnDoneJobCreatedPayload
	iferr.Panic(json.Unmarshal(event.Payload, &payload))
	p.checkJobIdIfAllGroupKeysAreDone(payload.JobId, payload.GroupKeys, map[string]bool{})
}

func (p *ProcessEvents) processJobDoneEvent(event Event) {
	var payload EventJobDonePayload
	iferr.Panic(json.Unmarshal(event.Payload, &payload))

	doneGroup := map[string]bool{} // cache to prevent to many db query

	// Get all job ids waiting for the same on-done group key
	var jobs []struct {
		OnDoneJobId uint64 `db:"job_id"`
	}
	qOnDoneSelectByGroupKey(p.db, payload.GroupKey, &jobs)
	for _, job := range jobs {
		// Get all on done groups waited by the job id
		var groups []struct {
			OnDoneGroupKey string `db:"group_key"`
		}
		qOnDoneSelectByJobId(p.db, job.OnDoneJobId, &groups)
		var onDoneGroupKeys []string
		for _, grp := range groups {
			onDoneGroupKeys = append(onDoneGroupKeys, grp.OnDoneGroupKey)
		}

		p.checkJobIdIfAllGroupKeysAreDone(job.OnDoneJobId, onDoneGroupKeys, doneGroup)
	}
}

func (p *ProcessEvents) checkJobIdIfAllGroupKeysAreDone(jobId uint64, onDoneGroupKeys []string, doneGroup map[string]bool) {

	// Check if all groups are done (use cache)
	allDone := true
	for _, onDoneGroupKey := range onDoneGroupKeys {
		done, ok := doneGroup[onDoneGroupKey]
		if !ok {
			undoneCount := qJobsCountUnDoneJobsByGroupKey(p.db, onDoneGroupKey)
			done = undoneCount == 0
			doneGroup[onDoneGroupKey] = done
		}
		if !done {
			allDone = false
			break
		}
	}

	if !allDone {
		return
	}

	doneJobIds := qJobsGetDoneJobIdsByGroupKeys(p.db, onDoneGroupKeys)

	// All groups done => set the on-done job to `ready` and remove unused entries in on done table
	qJobsUpdateStatusReadyAndDoneJobIds(p.db, jobId, doneJobIds)
	qOnDoneDeleteByJobId(p.db, jobId) // to keep table small
}

func (p *ProcessEvents) Stop()           { p.bg.Stop() }
func (p *ProcessEvents) IsRunning() bool { return p.bg.IsRunning() }
