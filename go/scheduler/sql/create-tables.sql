CREATE TABLE IF NOT EXISTS vs_logs
(
    id          BIGINT UNSIGNED NOT NULL PRIMARY KEY,
    instance_id VARCHAR(16)     NOT NULL,
    message     TEXT,
    log         TEXT
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS vs_registrations
(
    id          BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    instance_id VARCHAR(16)     NOT NULL,
    payload     TEXT
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS vs_jobs
(
    id           BIGINT UNSIGNED                   NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name         VARCHAR(200)                      NOT NULL,
    job_mode     ENUM ('standard', 'on-done')      NOT NULL,
    status       ENUM ('created', 'ready', 'done') NOT NULL,
    job_key      VARCHAR(200)                      NOT NULL,
    group_key    VARCHAR(200),
    payload      LONGTEXT,
    result       LONGTEXT,
    done_job_ids LONGTEXT,
    instance_id  VARCHAR(16),
    UNIQUE (name, job_key),
    INDEX (instance_id, status, id),
    INDEX (status, id),
    INDEX (group_key, status)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS vs_events
(
    id      BIGINT UNSIGNED          NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name    VARCHAR(200)             NOT NULL,
    payload TEXT,
    status  ENUM ('created', 'done') NOT NULL,
    INDEX (status, id)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS vs_on_done
(
    job_id    BIGINT UNSIGNED NOT NULL,
    group_key VARCHAR(200)    NOT NULL,
    FOREIGN KEY (job_id) REFERENCES vs_jobs (id) ON DELETE CASCADE,
    UNIQUE (job_id, group_key),
    INDEX (group_key)
) ENGINE = InnoDB;
