package scheduler

import (
	"sort"
)

func (sch *Scheduler) leaderInRunningState(nextId uint64) {
	if qRegistrationsAvailable(sch.db, sch.slog.LastRegId) != 0 {
		sch.slog.State = statePreparation
		sch.updateNextState(nextId, "updated registrations, next state: preparation")
		return
	}
	sch.leaderSetupAndTriggerAssignJobs() // start assign jobs
	sch.lProcessEvents.Start()            // start process events TODO need setup ?
	sch.sleep(durationUnit)
}

func (sch *Scheduler) leaderSetupAndTriggerAssignJobs() {
	var workerInstances []string
	for instanceId := range sch.slog.WorkersMap {
		workerInstances = append(workerInstances, instanceId)
	}
	sort.Slice(workerInstances, func(i, j int) bool {
		return workerInstances[i] < workerInstances[j]
	})
	sch.lAssignJobs.Start(sch.InstanceId, sch.slog.WorkersMap)
}

func (sch *Scheduler) workerInRunningState() {
	workerDetail := sch.slog.WorkersMap[sch.InstanceId]
	if len(workerDetail.AssignedRoles) != 0 {
		sch.wRunRole.Start(sch.InstanceId, workerDetail.AssignedRoles)
	}
	sch.wRunJob.Start(sch.InstanceId, workerDetail.Registration.Jobs)
	sch.sleep(durationUnit)
}
