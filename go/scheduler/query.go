package scheduler

import (
	"database/sql"
	_ "embed"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"

	"vostra/go/common"
	"vostra/go/iferr"
	"vostra/go/ref"
	"vostra/go/schdebug"
)

type Registration struct {
	Id         uint64
	InstanceId string `db:"instance_id"`
	Payload    json.RawMessage
}

var (
	//go:embed sql/create-tables.sql
	createTablesSQL string

	//go:embed sql/drop-tables.sql
	dropTablesSQL string
)

func qCreateTables(db *sqlx.DB) {
	db.MustExec(createTablesSQL)
}

func qDropTables(db *sqlx.DB) {
	db.MustExec(dropTablesSQL)
}

// logs

func qLogsGetLastId(db *sqlx.DB) (lastId uint64) {
	iferr.Panic(db.QueryRow("SELECT IFNULL(MAX(id), 0) FROM vs_logs").Scan(&lastId))
	return
}

func qLogsGetById(db *sqlx.DB, lastId uint64) (b []byte, err error) {
	err = db.QueryRow("SELECT log FROM vs_logs WHERE id = ?", lastId).Scan(&b)
	return
}

func qLogsInsertIgnore(db *sqlx.DB, id uint64, instanceId, message string, slog StateLog) {
	b := common.JsonMustMarshal(slog)

	tx, err := db.Begin()
	iferr.Panic(err)
	defer common.RecoverRollbackPanic(tx)

	_, err = tx.Exec(
		"INSERT IGNORE INTO vs_logs (id, instance_id, message, log) VALUES (?, ?, ?, ?)",
		id, instanceId, message, b)
	iferr.Panic(err)
	if id > 1 {
		_, err = tx.Exec("UPDATE vs_logs SET log = NULL WHERE id = ?", id-1)
		iferr.Panic(err)
	}
	iferr.Panic(tx.Commit())
}

// registrations

func qRegistrationsInsert(db *sqlx.DB, instanceId string, payload any) {
	b := common.JsonMustMarshal(payload)
	db.MustExec("INSERT vs_registrations (instance_id, payload) VALUES (?, ?)", instanceId, b)
}

func qRegistrationsSelectAndCallback(db *sqlx.DB, lastRegId uint64, cb func(reg Registration)) uint64 {
	rows, err := db.Queryx(
		"SELECT id, instance_id, payload FROM vs_registrations WHERE id > ? ORDER BY id",
		lastRegId)
	iferr.Panic(err)
	for rows.Next() {
		var reg Registration
		iferr.Panic(rows.StructScan(&reg))
		cb(reg)
		lastRegId = reg.Id
	}
	return lastRegId
}

func qRegistrationsAvailable(db *sqlx.DB, lastRegId uint64) (cnt int) {
	iferr.Panic(db.QueryRow("SELECT COUNT(*)FROM vs_registrations WHERE id > ?", lastRegId).Scan(&cnt))
	return
}

// jobs

func qJobsSetInvalidInstanceIdToNull(db *sqlx.DB, registeredInstanceId map[string]bool) {
	db.MustExec("UPDATE vs_jobs SET instance_id = NULL WHERE instance_id IS NOT NULL AND status='done'")

	nonexistent, qMarks := qJobsFindNonExistentInstanceIds(db, registeredInstanceId)
	if len(nonexistent) != 0 {
		schdebug.DBJobs.Printf("Found nonexistent instance ids: %v", nonexistent)
		db.MustExec(
			fmt.Sprintf(
				"UPDATE vs_jobs SET instance_id = NULL WHERE instance_id IN (%s)",
				strings.Join(qMarks, ",")),
			nonexistent...)
	}
}

func qJobsFindNonExistentInstanceIds(db *sqlx.DB, registeredInstanceId map[string]bool) (nonexistent []any, qMarks []string) {
	rows, err := db.Query("SELECT DISTINCT instance_id FROM vs_jobs WHERE instance_id IS NOT NULL")
	iferr.Panic(err)
	for rows.Next() {
		var instanceId string
		iferr.Panic(rows.Scan(&instanceId))
		if !registeredInstanceId[instanceId] {
			nonexistent = append(nonexistent, instanceId)
			qMarks = append(qMarks, "?")
		}
	}
	return
}

func qJobsSelectToAssign(db *sqlx.DB, limit int) (
	cntRows int,
	jobIdToName map[uint64]string,
	unassigned []uint64,
	assignedMap map[string][]uint64,
) {
	jobIdToName = map[uint64]string{}
	assignedMap = map[string][]uint64{}

	rows, err := db.Queryx(`
			SELECT id, name, instance_id FROM vs_jobs
			WHERE status = 'ready'
			ORDER BY id LIMIT ?`, limit)
	iferr.Panic(err)
	for rows.Next() {
		var job struct {
			Id         uint64
			Name       string
			InstanceId *string `db:"instance_id"`
		}
		iferr.Panic(rows.StructScan(&job))
		cntRows++
		jobIdToName[job.Id] = job.Name
		if job.InstanceId == nil {
			unassigned = append(unassigned, job.Id)
		} else {
			assignedMap[*job.InstanceId] = append(assignedMap[*job.InstanceId], job.Id)
		}
	}
	return
}

func qJobsAssign(db *sqlx.DB, jobId uint64, instanceId string) {
	schdebug.DBJobs.Printf("Assign job [%d] to instance '%s'", jobId, instanceId)
	db.MustExec("UPDATE vs_jobs SET instance_id = ? WHERE id = ?", instanceId, jobId)
}

func qJobsPickAssignedJob(db *sqlx.DB, instanceId string) *JobDB {
	var job JobDB
	err := db.QueryRowx(`
		SELECT id, name, job_key, group_key, done_job_ids, payload FROM vs_jobs
		WHERE instance_id = ? AND status = 'ready'
		ORDER BY id LIMIT 1`, instanceId).StructScan(&job)
	switch err {
	case nil:
		return &job
	case sql.ErrNoRows:
		return nil
	default:
		panic(err)
	}
}

func qJobsUpdateStatusReadyAndDoneJobIds(db *sqlx.DB, jobId uint64, doneJobIds []uint64) {
	schdebug.DBJobs.Printf("Update job [%d] 1) status to ready and 2) the done job ids", jobId)
	b := common.JsonMustMarshal(doneJobIds)
	db.MustExec("UPDATE vs_jobs SET status = 'ready', done_job_ids = ? WHERE id = ?", b, jobId)
}

func qJobsCountUnDoneJobsByGroupKey(db *sqlx.DB, groupKey string) (count int) {
	iferr.Panic(db.
		QueryRow("SELECT COUNT(*) FROM vs_jobs WHERE group_key = ? AND status != 'done'", groupKey).
		Scan(&count))
	schdebug.DBJobs.Printf("There is/are %d NOT DONE jobs with group key '%s'", count, groupKey)
	return
}

func qJobsGetDoneJobIdsByGroupKeys(db *sqlx.DB, groupKeys []string) (doneJobIds []uint64) {
	q, args, err := sqlx.In("SELECT id FROM vs_jobs WHERE group_key IN (?)", groupKeys)
	iferr.Panic(err)
	q = db.Rebind(q)
	rows, err := db.Query(q, args...)
	iferr.Panic(err)
	for rows.Next() {
		var id uint64
		iferr.Panic(rows.Scan(&id))
		doneJobIds = append(doneJobIds, id)
	}
	return
}

func qJobsGetJobInfoById(db *sqlx.DB, jobId uint64) *JobDB {
	var job JobDB
	err := db.Get(&job,
		"SELECT id, name, job_key, group_key, done_job_ids, result FROM vs_jobs WHERE id = ?", jobId)
	switch err {
	case nil:
		return &job
	case sql.ErrNoRows:
		return nil
	default:
		panic(err)
	}
}

// events

func qEventsGetOneUndoneEvent(db *sqlx.DB) *Event {
	var event Event
	err := db.
		QueryRowx("SELECT id, name, payload FROM vs_events WHERE status = 'created' ORDER BY id LIMIT 1").
		StructScan(&event)
	if err == nil {
		return &event
	}
	if err == sql.ErrNoRows {
		return nil
	}
	panic(err)
}

func qEventsUpdateStatusDone(db *sqlx.DB, eventId uint64) {
	schdebug.DBEvents.Printf("Update event [%d] status to done", eventId)
	db.MustExec("UPDATE vs_events SET status = 'done' WHERE id = ?", eventId)
}

// on done

func qOnDoneSelectByGroupKey(db *sqlx.DB, groupKey string, jobs any) {
	iferr.Panic(db.Select(jobs, "SELECT job_id FROM vs_on_done WHERE group_key = ?", groupKey))
}

func qOnDoneSelectByJobId(db *sqlx.DB, jobId uint64, groups any) {
	iferr.Panic(db.Select(groups, "SELECT group_key FROM vs_on_done WHERE job_id = ?", jobId))
}

func qOnDoneDeleteByJobId(db *sqlx.DB, jobId uint64) {
	schdebug.DBOnDone.Printf("Delete on-done job [%d]", jobId)
	db.MustExec("DELETE FROM vs_on_done WHERE job_id = ?", jobId)
}

// multi tables

func qCreateJobs(db *sqlx.DB, reqs []ref.CreateJobRequest) {
	tx, err := db.Beginx()
	iferr.Panic(err)
	defer common.RecoverRollbackPanic(tx)

	panicIfErrOtherThanDuplicate := func(err error) {
		if err != nil && !strings.HasPrefix(err.Error(), "Error 1062:") {
			panic(err)
		}
	}

	for _, req := range reqs {
		if len(req.OnDoneGroups) == 0 {

			// standard job

			rst, err := tx.Exec(`
				INSERT INTO vs_jobs ( name, job_mode, job_key, group_key, payload, status )
				VALUES (?, 'standard', ?, ?, ?, 'ready')`,
				req.JobName,
				common.IfEmptyUuid(req.JobKey),
				common.StringToSqlNullString(req.GroupKey),
				req.Payload)
			panicIfErrOtherThanDuplicate(err)
			if err != nil {
				//log.Println(err)
				continue
			}
			jobId, err := rst.LastInsertId()
			iferr.Panic(err)
			schdebug.DBJobs.Printf("Insert standard job [%d] '%s' with status=ready", jobId, req.JobName)

		} else {

			// on-done job

			rst, err := tx.Exec(`
				INSERT INTO vs_jobs ( name, job_mode, job_key, group_key, payload, status )
				VALUES (?, 'on-done', ?, ?, ?, 'created')`,
				req.JobName,
				common.IfEmptyUuid(req.JobKey),
				common.StringToSqlNullString(req.GroupKey),
				req.Payload)
			panicIfErrOtherThanDuplicate(err)
			if err != nil {
				//log.Println(err)
				continue
			}
			jobId, err := rst.LastInsertId()
			iferr.Panic(err)
			schdebug.DBJobs.Printf(
				"Insert on-done job [%d] '%s' with on-done groups: %v and status=created",
				jobId, req.JobName, req.OnDoneGroups)

			for _, onDoneGroupKey := range req.OnDoneGroups {
				tx.MustExec("INSERT INTO vs_on_done (job_id, group_key) VALUES (?, ?)",
					jobId, onDoneGroupKey)
			}

			b := common.JsonMustMarshal(EventOnDoneJobCreatedPayload{JobId: uint64(jobId), GroupKeys: req.OnDoneGroups})
			schdebug.DBEvents.Printf("Insert event '%s', payload: '%s'", eventOnDoneJobCreated, b)
			tx.MustExec("INSERT INTO vs_events (name, payload, status) VALUES (?, ?, 'created')",
				eventOnDoneJobCreated, b)
		}
	}

	iferr.Panic(tx.Commit())
}

func qUpdateJobResult(db *sqlx.DB, jobId uint64, result []byte) {
	tx, err := db.Beginx()
	iferr.Panic(err)
	defer common.RecoverRollbackPanic(tx)

	var groupKey sql.NullString
	iferr.Panic(tx.QueryRow("SELECT group_key FROM vs_jobs WHERE id = ? FOR UPDATE", jobId).Scan(&groupKey))

	schdebug.DBJobs.Printf("Update job [%d] status to done and result to '%s'", jobId, result)
	tx.MustExec("UPDATE vs_jobs SET result = ?, status = 'done', instance_id = NULL WHERE id = ?", result, jobId)

	if groupKey.String != "" {
		b := common.JsonMustMarshal(EventJobDonePayload{JobId: jobId, GroupKey: groupKey.String})
		schdebug.DBEvents.Printf("Insert new event '%s' with payload '%s'", eventJobDone, b)
		tx.MustExec("INSERT INTO vs_events (name, payload, status) VALUES (?, ?, 'created')",
			eventJobDone, b)
	}

	iferr.Panic(tx.Commit())
}
