package algo

import (
	"sort"

	"vostra/go/schdebug"
)

type InstanceJobName struct {
	InstanceId string
	JobName    string
}

type InstanceJobIds struct {
	InstanceId string
	JobIds     []uint64
}

func AssignJobs(
	instances []string,
	registration map[InstanceJobName]bool,
	jobIdToName map[uint64]string,
	unassigned []uint64,
	assignedMap map[string][]uint64,
	maxQueue int,
	assignJob func(jobId uint64, instanceId string),
	canNotAssign func(jobId uint64),
) {
	// Create entries in assignedMap for missing instance ids
	for _, id := range instances {
		if _, ok := assignedMap[id]; !ok {
			assignedMap[id] = nil
		}
	}

	// Transform assignedMap to assignedList for sorting
	var assignedList []*InstanceJobIds
	for id, jobs := range assignedMap {
		assignedList = append(assignedList, &InstanceJobIds{InstanceId: id, JobIds: jobs})
	}

	for _, jobId := range unassigned {
		jobName := jobIdToName[jobId]
		schdebug.AssignJobs.Printf("Checking job [%d] '%s' for assignment", jobId, jobName)

		// Sort assignedList with the least jobs at index 0
		sort.Slice(assignedList, func(i, j int) bool {
			return len(assignedList[i].JobIds) < len(assignedList[j].JobIds)
		})

		// Iterate to each instance in assignedList to assign the job to the instance if
		// the instance is registered to the job.
		// Repeat until can not find instance with jobs queue less than maxQueue
		for i := 0; i < len(instances); i++ {
			instanceId := assignedList[i].InstanceId
			schdebug.AssignJobs.Printf("Check instance '%s' for job [%d]", instanceId, jobId)
			if len(assignedList[i].JobIds) >= maxQueue {
				schdebug.AssignJobs.Printf("Max queue, can NOT assign job [%d]", jobId)
				canNotAssign(jobId)
				break
			}
			if registration[InstanceJobName{instanceId, jobName}] {
				assignedList[i].JobIds = append(assignedList[i].JobIds, jobId)
				schdebug.AssignJobs.Printf("Assign job [%d] to instance '%s'", jobId, instanceId)
				assignJob(jobId, instanceId)
				break
			}
		}
	}
}
