package algo

// AssignRoles assign each role to available instances
func AssignRoles(
	instances []string,
	instanceToRoles map[string][]string,
	assign func(role, instanceId string),
) {
	roleAssigned := map[string]bool{}
	for {
		isAssigned := false
		for _, instanceId := range instances {
			var roles []string
			for _, role := range instanceToRoles[instanceId] {
				if !roleAssigned[role] {
					roles = append(roles, role)
				}
			}
			if len(roles) > 0 {
				role := roles[0]
				roles = roles[1:]
				assign(role, instanceId)
				roleAssigned[role] = true
				isAssigned = true
			}
			instanceToRoles[instanceId] = roles
		}
		if !isAssigned {
			break
		}
	}
}
