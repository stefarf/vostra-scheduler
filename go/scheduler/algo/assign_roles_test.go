package algo

import (
	"fmt"
	"strings"
	"testing"
)

func TestAssignRoles(t *testing.T) {
	// Given
	var instances = []string{"i1", "i2", "i3"}
	var instanceToRoles = map[string][]string{
		"i1": {"a", "b"},
		"i2": {"a", "c"},
		"i3": {"a", "b", "c", "d"},
	}

	// When
	var got []string
	AssignRoles(instances, instanceToRoles, func(role, instanceId string) {
		got = append(got, fmt.Sprintf("assign role %s to %s", role, instanceId))
	})

	// Then
	want := []string{
		"assign role a to i1",
		"assign role c to i2",
		"assign role b to i3",
		"assign role d to i3",
	}
	gotStr := strings.Join(got, "\n")
	wantStr := strings.Join(want, "\n")
	if gotStr != wantStr {
		t.Errorf("got:\n\n%s\n\nwant:\n\n%s\n\n", gotStr, wantStr)
	}
}
