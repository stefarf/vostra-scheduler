package algo

import (
	"fmt"
	"strings"
	"testing"
)

func TestAssignJobs(t *testing.T) {
	maxQueue := 3
	instances := []string{"i1", "i2", "i3", "i4"}
	registration := map[InstanceJobName]bool{
		{"i1", "j1"}: true,
		{"i1", "j2"}: true,

		{"i2", "j1"}: true,
		{"i2", "j3"}: true,

		{"i3", "j3"}: true,
		{"i4", "j3"}: true,
	}
	assignedMap := map[string][]uint64{
		"i1": {1, 2},
		"i2": {3},
		"i3": {4, 5},
	}
	unassigned := []uint64{6, 7, 8, 9, 10, 11, 12}
	jobIdToName := map[uint64]string{
		6:  "j1",
		7:  "j2",
		8:  "j1",
		9:  "j1",
		10: "j3",
		11: "j1",
		12: "j3",
	}

	var got []string
	AssignJobs(
		instances, registration, jobIdToName, unassigned, assignedMap, maxQueue,
		func(jobId uint64, instanceId string) {
			if len(assignedMap[instanceId]) >= maxQueue {
				t.Error("can not assigned if queue is max")
			}
			jobName := jobIdToName[jobId]
			if !registration[InstanceJobName{instanceId, jobName}] {
				t.Error("can not assigned to unregistered instance")
			}
			assignedMap[instanceId] = append(assignedMap[instanceId], jobId)
			got = append(got, fmt.Sprintf("assign %d to %s", jobId, instanceId))
		},
		func(jobId uint64) {
			got = append(got, fmt.Sprintf("can not assign %d", jobId))
		})
	want := []string{
		"assign 6 to i2",
		"assign 7 to i1",
		"assign 8 to i2",
		"can not assign 9",
		"assign 10 to i4",
		"can not assign 11",
		"assign 12 to i4",
	}
	gotStr := strings.Join(got, "\n")
	wantStr := strings.Join(want, "\n")
	if gotStr != wantStr {
		t.Errorf("got:\n\n%s\n\nwant:\n\n%s\n\n", gotStr, wantStr)
	}
}
