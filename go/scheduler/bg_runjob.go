package scheduler

import (
	"log"
	"time"

	"github.com/jmoiron/sqlx"

	"vostra/go/common"
	"vostra/go/ref"
	"vostra/go/schdebug"
	"vostra/go/scheduler/runbg"
)

type RunJob struct {
	db         *sqlx.DB
	bg         *runbg.RunBg
	instanceId string
	appPort    int
	opt        Options
}

func newRunJob(db *sqlx.DB, instanceId string, appPort int, opt Options) *RunJob {
	return &RunJob{
		db:         db,
		bg:         runbg.New(durationUnit / 3),
		instanceId: instanceId,
		appPort:    appPort,
		opt:        opt,
	}
}

func (j *RunJob) Start(instanceId string, jobHandlers []ref.RegisterJobRequest) {
	j.bg.Start(func() time.Duration {
		jobNameToEndpoint := map[string]string{}
		for _, h := range jobHandlers {
			jobNameToEndpoint[h.Job] = h.Endpoint
		}

		picked := qJobsPickAssignedJob(j.db, j.instanceId)
		if picked == nil {
			return j.opt.DurationNoJobAssigned
		}
		endpoint, ok := jobNameToEndpoint[picked.Name]
		if !ok {
			log.Println(instanceId, "Error can not find registered job name:", picked.Name)
			return j.opt.DurationNoJobAssigned
		}
		schdebug.RunJob.Printf("Execute job [%d] '%s'", picked.Id, picked.Name)

		payload := jobDbToPayload(*picked)
		payloadB := common.JsonMustMarshal(payload)
		result := defaultCallJobHandler(instanceId, endpoint, j.appPort, payloadB)
		qUpdateJobResult(j.db, picked.Id, result)
		return 0
	})
}

func (j *RunJob) Stop()           { j.bg.Stop() }
func (j *RunJob) IsRunning() bool { return j.bg.IsRunning() }
