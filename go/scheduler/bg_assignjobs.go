package scheduler

import (
	"time"

	"github.com/jmoiron/sqlx"

	"vostra/go/scheduler/algo"
	"vostra/go/scheduler/runbg"
)

type AssignJobs struct {
	db  *sqlx.DB
	bg  *runbg.RunBg
	opt Options
}

func newAssignJobs(db *sqlx.DB, opt Options) *AssignJobs {
	return &AssignJobs{db: db, bg: runbg.New(durationUnit / 3), opt: opt}
}

func (a *AssignJobs) Start(leaderInstanceId string, workersMap map[InstanceId]*WorkerDetail) {
	a.bg.Start(func() time.Duration {
		var instances []string
		registration := map[algo.InstanceJobName]bool{}

		for wi, wd := range workersMap {
			instances = append(instances, wi)
			for _, reg := range wd.Registration.Jobs {
				registration[algo.InstanceJobName{InstanceId: wi, JobName: reg.Job}] = true
			}
		}

		registeredInstanceId := map[string]bool{}
		for _, i := range instances {
			registeredInstanceId[i] = true
		}
		qJobsSetInvalidInstanceIdToNull(a.db, registeredInstanceId)

		limit := a.opt.JobsMaxQueue*len(instances) + a.opt.JobsRemaining
		cntRows, jobIdToName, unassigned, assignedMap := qJobsSelectToAssign(a.db, limit)

		var maxQueue int
		if cntRows == limit {
			maxQueue = a.opt.JobsMaxQueue
		} else {
			maxQueue = 1
		}
		algo.AssignJobs(
			instances,
			registration,
			jobIdToName,
			unassigned,
			assignedMap,
			maxQueue,
			func(jobId uint64, instanceId string) {
				qJobsAssign(a.db, jobId, instanceId)
			},
			func(jobId uint64) {}, // canNotAssign
		)

		if cntRows == limit {
			return a.opt.DurationFullJobQ
		} else {
			return a.opt.DurationLessJobQ
		}
	})
}

func (a *AssignJobs) Stop()           { a.bg.Stop() }
func (a *AssignJobs) IsRunning() bool { return a.bg.IsRunning() }
