package scheduler

import (
	"fmt"

	"vostra/go/common"
	"vostra/go/ref"
)

func (sch *Scheduler) Register(req ref.RegisterRequest) {
	epMap := map[string]bool{}
	roleMap := map[string]bool{}
	jobMap := map[string]bool{}

	// Validate
	for _, r := range req.Roles {
		if epMap[r.Endpoint] {
			panic(fmt.Sprintf("duplicate endpoint: %s", r.Endpoint))
		}
		epMap[r.Endpoint] = true

		if roleMap[r.Role] {
			panic(fmt.Sprintf("duplicate role: %s", r.Role))
		}
		roleMap[r.Role] = true
	}
	for _, j := range req.Jobs {
		if epMap[j.Endpoint] {
			panic(fmt.Sprintf("duplicate endpoint: %s", j.Endpoint))
		}
		epMap[j.Endpoint] = true

		if jobMap[j.Job] {
			panic(fmt.Sprintf("duplicate job: %s", j.Job))
		}
		jobMap[j.Job] = true
	}

	// Update registration
	qRegistrationsInsert(sch.db, sch.InstanceId, req)
}

func (sch *Scheduler) CreateJobs(reqs []ref.CreateJobRequest) {
	wd := sch.slog.WorkersMap[sch.InstanceId]
	if wd == nil {
		panic(fmt.Sprintf("instance '%s' is not a worker, may not create job", sch.InstanceId))
	}

	// Validate job names
	validJobName := map[string]bool{}
	for _, reg := range wd.Registration.Jobs {
		validJobName[reg.Job] = true
	}
	for _, req := range reqs {
		if !validJobName[req.JobName] {
			panic(fmt.Sprintf("invalid job '%s'", req.JobName))
		}
	}

	qCreateJobs(sch.db, reqs)
}

func (sch *Scheduler) GetJobInfo(jobId uint64) []byte {
	job := qJobsGetJobInfoById(sch.db, jobId)
	if job == nil {
		return nil
	}
	return common.JsonMustMarshal(jobDbToResult(*job))
}
