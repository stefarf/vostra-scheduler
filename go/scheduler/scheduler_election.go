package scheduler

import "time"

func (sch *Scheduler) leaderInElectionState(nextId uint64) {
	now := time.Now()

	sch.slog.State = statePreparation
	sch.slog.PrepWaitUntil = now.Add(sch.opt.DurationPreparation)
	for _, wd := range sch.slog.WorkersMap {
		wd.ConfirmStop = false
	}
	sch.updateNextState(nextId, "leader elected and next state: preparation")
}
