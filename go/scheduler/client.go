package scheduler

import (
	"fmt"
	"net/http"

	"github.com/go-resty/resty/v2"
)

var client = resty.New()

// defaultCallJobHandler is for testing
var defaultCallJobHandler = clientCallJobHandler

func clientCallRoleHandler(instanceId, endpoint, role string, appPort int) {
	res, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("X-Role", role).
		Post(fmt.Sprintf("http://localhost:%d%s", appPort, endpoint))
	if err != nil {
		panic(fmt.Sprintf(
			"error role handler: role '%s', endpoint '%s', port %d, error %v",
			role, endpoint, appPort, err))
	}
	if res.StatusCode() != http.StatusOK {
		panic(fmt.Sprintf(
			"error role handler: status code %d, role '%s', endpoint '%s', port %d",
			res.StatusCode(), role, endpoint, appPort))
	}
}

func clientCallJobHandler(instanceId, endpoint string, appPort int, jobInfo []byte) (result []byte) {
	res, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(jobInfo).
		Post(fmt.Sprintf("http://localhost:%d%s", appPort, endpoint))
	if err != nil {
		panic(fmt.Sprintf(
			"error job handler: endpoint '%s', port %d, error %v",
			endpoint, appPort, err))
	}
	if res.StatusCode() != http.StatusOK {
		panic(fmt.Sprintf(
			"error job handler: status code %d, endpoint '%s', port %d",
			res.StatusCode(), endpoint, appPort))
	}
	return res.Body()
}
