# Specifications

## Introduction

### State

- Election: Any instance will propose self as a leader if there is no active leader. Leader in this state will set the
  state to next one (preparation).
- Preparation: Leader will wait for all the workers to stop running (roles and jobs) for certain interval. If there is
  any worker running, then it will wait again for certain interval. It will keep doing this forever until all workers
  are stopped running.
- Assignment: Leader reads all pending events and update workers registration. Leader may remove existing assignments
  from workers (roles and jobs). It is safe to do so because all workers are not running.
- Running: Worker may start to run the role handler and jobs based on the assignment.

## Specs

### Jobs API

- Create job:
    - name, job_key, group_key, payload
- Create on-done-job:
    - name, job_key, group_key, done_groups, done_mode (all groups, any group)
    - payload = {jobDone:[id, ...], jobNotDone:[id, ...]}
- Get job info (id): name, job_key, group_key, payload, status, result

Trigger: 1) saat job done, 2) saat create on-done-job

```text
[Worker] Create `standard` job:
    insert into vs_jobs: name, jobkey, groupkey, payload, jobmode=standard, status=ready

[Worker] Done `standard` job:
    tx:
      select groupkey as donegroup from vs_jobs where id=<doneid> for update    // get group of the just done job
      update vs_jobs: status=done where id=<doneid>                             // update status done
      insert vs_event: name=jobdone, payload={jobid:doneid, groupkey:donegroup}, status=created

[Worker] Create `ondone` job:
    tx:
      insert into vs_jobs: name, jobkey, groupkey, payload, jobmode=ondone, status=created
      insert into vs_on_done_groups: groupkey, jobid
      insert into vs_on_done_groups: groupkey, jobid
      ...
      insert vs_event: name=createondonejob, payload={jobid, groupkeys:[ ... ]}, status=created
      
[Leader] Process events:
    select * from vs_events where status=created order by id limit 1
    if event.name == jobdone:
        clear gropu done cache // cache per event
        
        donegroup = event.payload.groupkey
        
        // get all job ids impacted by done group
        select jobid as ondoneids from vs_on_done_groups where groupkey=<donegroup> 
        
        for jobid in ondoneids:
          // process each impacted job id
          
          select groupkey as alldonegroups from vs_on_done_groups where jobid=<jobid>
          
          cache group key done, per groupkey
          
          select count(*) from vs_jobs where groupkey in <alldonegroups> and status!=done
          if count == 0: // if all done
            update vs_jobs: status=ready where jobid=<jobid> and status!=done // ready to assign
            delete from vs_on_done_groups where jobid=<jobid> // to ensure performance
            
```