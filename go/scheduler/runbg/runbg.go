package runbg

import (
	"time"

	"vostra/go/common"
)

type RunBg struct {
	running      bool
	askStop      bool
	chStopped    chan bool
	durationStep time.Duration
}

func New(durationStep time.Duration) *RunBg {
	return &RunBg{durationStep: durationStep}
}

// Start starts the go routine
func (j *RunBg) Start(bg func() time.Duration) {
	if j.running {
		return
	}
	j.running = true
	j.askStop = false
	j.chStopped = make(chan bool)

	go func() {
		defer func() {
			j.chStopped <- true
		}()
		for {
			dur := time.Second * 30 // default duration if bg() panics
			func() {
				defer common.RecoverAndLog()
				dur = bg()
			}()
			next := time.Now().Add(dur)
			for time.Now().Before(next) {
				if j.askStop {
					return
				}
				time.Sleep(j.durationStep)
			}
		}
	}()
}

// Stop asks the go routine to stop and will block until it is stopped
func (j *RunBg) Stop() {
	if !j.running {
		return
	}
	j.askStop = true
	<-j.chStopped // wait for job to stop
	j.askStop = false
	j.running = false
}

func (j *RunBg) IsRunning() bool { return j.running }
