package runbg

import (
	"testing"
	"time"
)

func TestRunBg(t *testing.T) {
	cntRun := 0
	bg := func() time.Duration { cntRun++; return time.Millisecond * 20 }

	r := New(time.Millisecond * 5)
	r.Start(bg)
	r.Start(bg)
	time.Sleep(time.Millisecond * 5)
	if cntRun != 1 {
		t.Errorf("expect to run 1 time, not %d time(s)", cntRun)
	}
	r.Stop()
}
