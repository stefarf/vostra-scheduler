package scheduler

import (
	"time"

	"vostra/go/ref"
)

type (
	InstanceId = string

	StateLog struct {
		Leader struct {
			ValidUntil time.Time
			InstanceId string
		}
		State         string
		WorkersMap    map[InstanceId]*WorkerDetail
		PrepWaitUntil time.Time
		LastRegId     uint64
	}

	WorkerDetail struct {
		ConfirmStop   bool                      // for worker to confirm stop in preparation state
		Registration  *ref.RegisterRequest      // record latest worker's registration
		AssignedRoles []ref.RegisterRoleRequest // roles assigned to instance
	}
)
