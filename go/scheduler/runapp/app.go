package runapp

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"sync"
)

type (
	RunApp struct {
		name          string
		args          []string
		schedulerPort int
		port          int

		run struct {
			sync.Mutex
			cmd *exec.Cmd
		}

		// OnStop will be called when the app exits/stops
		OnStop func()
	}

	Runner interface {
		Start(instanceId string)
		Stop() error
		Port() int
	}
)

var _ Runner = &RunApp{}

func New(name string, args []string, schedulerPort, appPort int) *RunApp {
	return &RunApp{
		name:          name,
		args:          args,
		schedulerPort: schedulerPort,
		port:          appPort,
	}
}

// Start executes the app if it is not running otherwise it does nothing.
// It is safe for concurrent access.
func (app *RunApp) Start(instanceId string) {
	app.run.Lock()
	defer app.run.Unlock()

	if app.run.cmd != nil {
		//log.Println("App is running with pid:", app.run.cmd.Process.Pid)
		return
	}

	app.run.cmd = exec.Command(app.name, app.args...)
	app.run.cmd.Stdout = os.Stdout
	app.run.cmd.Stderr = os.Stderr
	app.run.cmd.Env = append(os.Environ(),
		fmt.Sprintf("INSTANCE_ID=%s", instanceId),
		fmt.Sprintf("SCHEDULER_PORT=%d", app.schedulerPort),
		fmt.Sprintf("APP_PORT=%d", app.port),
	)

	go func() {
		err := app.run.cmd.Run()
		if err != nil {
			log.Println(err)
		}
		log.Println("App exited / stopped")
		if app.OnStop != nil {
			app.OnStop()
		}

		app.run.Lock()
		defer app.run.Unlock()
		app.run.cmd = nil
	}()
}

// Stop kills the app if it is running, otherwise it does nothing.
// It is safe for concurrent access.
func (app *RunApp) Stop() error {
	app.run.Lock()
	defer app.run.Unlock()
	if app.run.cmd == nil {
		return nil
	}
	log.Printf("Kill app: pid %d, port %d", app.run.cmd.Process.Pid, app.port)
	return app.run.cmd.Process.Kill()
}

func (app *RunApp) Port() int { return app.port }
