package scheduler

func jobDbToPayload(job JobDB) JobPayload {
	body := JobPayload{
		JobId:   job.Id,
		JobName: job.Name,
		JobKey:  job.Key,
	}
	if job.GroupKey.Valid {
		s := job.GroupKey.String
		body.GroupKey = &s
	}
	if job.DoneJobIds.Valid {
		body.DoneJobIds = []byte(job.DoneJobIds.String)
	}
	if job.Payload.Valid {
		body.Payload = []byte(job.Payload.String)
	}
	return body
}

func jobDbToResult(job JobDB) JobResult {
	info := JobResult{
		JobId:   job.Id,
		JobName: job.Name,
		JobKey:  job.Key,
	}
	if job.GroupKey.Valid {
		s := job.GroupKey.String
		info.GroupKey = &s
	}
	if job.DoneJobIds.Valid {
		info.DoneJobIds = []byte(job.DoneJobIds.String)
	}
	if job.Result.Valid {
		info.Result = []byte(job.Result.String)
	}
	return info
}
