package scheduler

import (
	"sort"

	"vostra/go/ref"
	"vostra/go/scheduler/algo"
)

func (sch *Scheduler) leaderInAssignmentState(nextId uint64) {
	// Assign roles in this state. Assign jobs not in this state but in running state.
	leaderAssignRolesAndUpdateWorkersMap(sch.slog.WorkersMap)

	sch.slog.State = stateRunning
	sch.updateNextState(nextId, "assign and next state: running")
}

func leaderAssignRolesAndUpdateWorkersMap(workersMap map[InstanceId]*WorkerDetail) {
	// Populate workerInstances, instanceToRoles, roleToRequest
	var workerInstances []string
	instanceToRoles := map[string][]string{}
	roleToRequest := map[string]ref.RegisterRoleRequest{}
	for wi, wd := range workersMap {
		workerInstances = append(workerInstances, wi)
		for _, r := range wd.Registration.Roles {
			instanceToRoles[wi] = append(instanceToRoles[wi], r.Role)
			roleToRequest[r.Role] = r
		}
	}

	// Sort to make the assignment deterministic
	sort.Slice(workerInstances, func(i, j int) bool {
		return workerInstances[i] < workerInstances[j]
	})

	// Assign roles
	for _, wd := range workersMap {
		wd.AssignedRoles = nil
	}
	algo.AssignRoles(workerInstances, instanceToRoles, func(role, instanceId string) {
		workersMap[instanceId].AssignedRoles = append(
			workersMap[instanceId].AssignedRoles, roleToRequest[role])
	})
}
