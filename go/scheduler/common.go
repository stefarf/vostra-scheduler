package scheduler

import (
	"database/sql"
	"encoding/json"
	"time"

	"vostra/go/scheduler/runrole"
)

type EventJobDonePayload struct {
	JobId    uint64
	GroupKey string
}

type EventOnDoneJobCreatedPayload struct {
	JobId     uint64
	GroupKeys []string
}

type Event struct {
	Id      uint64
	Name    string
	Payload json.RawMessage
}

// JobDB is for getting job data from db
type JobDB struct {
	Id         uint64
	Name       string
	Key        string         `db:"job_key"`
	GroupKey   sql.NullString `db:"group_key"`
	DoneJobIds sql.NullString `db:"done_job_ids"`
	Payload    sql.NullString
	Result     sql.NullString
}

// JobPayload is a translation from the JobDB
type JobPayload struct {
	JobId      uint64
	JobName    string
	JobKey     string
	GroupKey   *string
	DoneJobIds json.RawMessage
	Payload    json.RawMessage
}

// JobResult is a translation from the JobDB
type JobResult struct {
	JobId      uint64
	JobName    string
	JobKey     string
	GroupKey   *string
	DoneJobIds json.RawMessage
	Result     json.RawMessage
}

const (
	stateElection    = "election"
	statePreparation = "preparation"
	stateAssignment  = "assignment"
	stateRunning     = "running"
)

const (
	eventJobDone          = "jobDone"
	eventOnDoneJobCreated = "onDoneJobCreated"
)

var (
	durationUnit             time.Duration
	durationLeaderValidUntil time.Duration
	durationLimitToExtend    time.Duration
)

func init() {
	initDuration(time.Second)
}

func initDuration(unit time.Duration) {
	durationUnit = unit
	durationLeaderValidUntil = durationUnit * 30
	durationLimitToExtend = durationUnit * 3
}

// defaultNewRunRole is for testing
var defaultNewRunRole = func(appPort int, apiCall func(instanceId, endpoint, role string, appPort int)) runrole.Runner {
	return runrole.New(appPort, apiCall)
}
