package scheduler

import (
	"database/sql"
	"encoding/json"
	"math/rand"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"

	"vostra/go/common"
	"vostra/go/iferr"
	"vostra/go/schdebug"
	"vostra/go/scheduler/runapp"
	"vostra/go/scheduler/runrole"
)

type Scheduler struct {
	opt            Options
	db             *sqlx.DB
	wRunApp        runapp.Runner
	wRunRole       runrole.Runner
	wRunJob        *RunJob
	lAssignJobs    *AssignJobs
	lProcessEvents *ProcessEvents
	InstanceId     string

	slog StateLog
}

type Options struct {
	NumberOfInstances int
	AppPortStartFrom  int

	DurationPreparation   time.Duration
	DurationFullJobQ      time.Duration
	DurationLessJobQ      time.Duration
	DurationNoJobAssigned time.Duration
	DurationNoMoreEvent   time.Duration
	JobsMaxQueue          int
	JobsRemaining         int
}

func CreateTables(db *sqlx.DB, drop bool) {
	if drop {
		qDropTables(db)
	}
	qCreateTables(db)
}

func StartScheduler(
	opt Options,
	db *sqlx.DB,
	createAppRunner func(appPort int) runapp.Runner,
	schedulerCreated func(instanceId string, sch *Scheduler),
) {
	// Run scheduler instances
	appPort := opt.AppPortStartFrom
	for i := 0; i < opt.NumberOfInstances; i++ {
		app := createAppRunner(appPort)
		instanceId := createInstanceId(app.Port())
		sch := &Scheduler{
			opt:            opt,
			db:             db,
			wRunApp:        app,
			wRunRole:       defaultNewRunRole(appPort, clientCallRoleHandler),
			wRunJob:        newRunJob(db, instanceId, appPort, opt),
			lAssignJobs:    newAssignJobs(db, opt),
			lProcessEvents: newProcessEvents(db, opt),
			InstanceId:     instanceId,
		}
		schedulerCreated(instanceId, sch)
		appPort++
		go func() {
			for {
				sch.runAndRecover()
				time.Sleep(time.Second * 5)
			}
		}()
	}
}

func (sch *Scheduler) instanceStateLogln(v ...any) {
	schdebug.InstanceState.Println(append([]any{sch.InstanceId}, v...)...)
}

func (sch *Scheduler) instanceStateLogf(format string, v ...any) {
	schdebug.InstanceState.Printf("%s "+format, append([]any{sch.InstanceId}, v...)...)
}

func (sch *Scheduler) sleep(dur time.Duration) {
	dur += dur * time.Duration(rand.Int63n(20)/100)
	time.Sleep(dur)
}

func (sch *Scheduler) runAndRecover() {
	defer common.RecoverAndLog()
	for {
		lastId := qLogsGetLastId(sch.db)
		sch.instanceStateLogf("last id: %d", lastId)

		b, err := qLogsGetById(sch.db, lastId)
		switch err {

		case nil:
			// Need to reset before unmarshal. Is it Json BUG ?
			// This is to ensure unmarshal to recreate the variable, instead of reusing existing,
			// especially for the map content.
			sch.slog = StateLog{}

			if len(b) == 0 {
				continue
			}
			iferr.Panic(json.Unmarshal(b, &sch.slog))
			sch.takeActionBasedOnCurrentState(lastId + 1)

		case sql.ErrNoRows:
			// The vs_logs is currently empty, means that there is no state
			// Need to insert new state => propose self as leader
			sch.proposeSelfAsLeaderAndUpdateNextState(lastId + 1)

		default:
			panic(err)
		}
	}
}

func (sch *Scheduler) updateNextState(nextId uint64, message string) {
	sch.instanceStateLogln(message)
	qLogsInsertIgnore(sch.db, nextId, sch.InstanceId, message, sch.slog)
}

func (sch *Scheduler) proposeSelfAsLeaderAndUpdateNextState(nextId uint64) {
	sch.slog.Leader.ValidUntil = time.Now().Add(durationLeaderValidUntil)
	sch.slog.Leader.InstanceId = sch.InstanceId
	sch.slog.State = stateElection
	if sch.slog.WorkersMap == nil {
		sch.slog.WorkersMap = map[InstanceId]*WorkerDetail{}
	}
	sch.updateNextState(nextId, "propose self as leader")
}

func (sch *Scheduler) leaderExtendLeadershipAndUpdateNextState(nextId uint64) {
	sch.slog.Leader.ValidUntil = time.Now().Add(durationLeaderValidUntil)
	sch.updateNextState(nextId, "extend leadership")
}

func (sch *Scheduler) takeActionBasedOnCurrentState(nextId uint64) {
	now := time.Now()

	// Propose self as leader if there is no leader
	if sch.slog.Leader.ValidUntil.Before(now) {
		// Leader is expired
		sch.proposeSelfAsLeaderAndUpdateNextState(nextId)
		return
	}

	isLeader := sch.InstanceId == sch.slog.Leader.InstanceId

	// Extend leadership
	if isLeader && (sch.slog.Leader.ValidUntil.Sub(now) < durationLimitToExtend) {
		sch.leaderExtendLeadershipAndUpdateNextState(nextId)
		return
	}

	// Start / stop bg jobs
	// Create / delete WorkersMap[InstanceId]
	if isLeader {
		sch.instanceStateLogf("LEADER at state: '%s'", sch.slog.State)

		if _, ok := sch.slog.WorkersMap[sch.InstanceId]; ok {
			delete(sch.slog.WorkersMap, sch.InstanceId)
			sch.updateNextState(nextId, "delete self from workers map")
			return
		}

		// Stop app
		iferr.Println(sch.wRunApp.Stop())

		// Stop bg jobs
		if sch.wRunRole.IsRunning() || sch.wRunJob.IsRunning() {
			var wg sync.WaitGroup
			wg.Add(2)
			go func() {
				defer wg.Done()
				sch.wRunRole.Stop()
			}()
			go func() {
				defer wg.Done()
				sch.wRunJob.Stop()
			}()
			wg.Wait()
			// Because the stopping processes above may take long time to complete,
			// to make sure the state is still up-to-date, need to return.
			return
		}
	} else {
		sch.instanceStateLogf("WORKER at state '%s'", sch.slog.State)

		if _, ok := sch.slog.WorkersMap[sch.InstanceId]; !ok {
			sch.slog.WorkersMap[sch.InstanceId] = &WorkerDetail{}
			sch.updateNextState(nextId, "register self to workers map")
			return
		}

		// Start app
		sch.wRunApp.Start(sch.InstanceId)

		// Stop bg jobs
		if sch.lAssignJobs.IsRunning() || sch.lProcessEvents.IsRunning() {
			var wg sync.WaitGroup
			wg.Add(2)
			go func() {
				defer wg.Done()
				sch.lAssignJobs.Stop()
			}()
			go func() {
				defer wg.Done()
				sch.lProcessEvents.Stop()
			}()
			wg.Wait()
			// Because the stopping processes above may take long time to complete,
			// to make sure the state is still up-to-date, need to return.
			return
		}
	}

	// Take action based on current state
	switch sch.slog.State {
	case stateElection:
		if !isLeader {
			sch.sleep(durationUnit)
			return
		}
		sch.leaderInElectionState(nextId)
		return

	case statePreparation:
		if !isLeader {
			sch.workerInPreparationState(nextId)
			return
		}
		sch.leaderInPreparationState(nextId)
		return

	case stateAssignment:
		if !isLeader {
			sch.sleep(durationUnit)
			return
		}
		sch.leaderInAssignmentState(nextId)
		return

	case stateRunning:
		if !isLeader {
			sch.workerInRunningState()
			return
		}
		sch.leaderInRunningState(nextId)
		return

	default:
		panic("Unknown state")
	}
}
