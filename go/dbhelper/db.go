package dbhelper

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

func ConnectAndInit(host string, port int, dbName, user, passEnv string) (db *sqlx.DB) {
	dbCfg := mysql.Config{
		Net:                  "tcp",
		Addr:                 fmt.Sprintf("%s:%d", host, port),
		DBName:               dbName,
		User:                 user,
		Passwd:               os.Getenv(passEnv),
		Loc:                  getLocation("Asia/Jakarta"),
		AllowNativePasswords: true,
		MultiStatements:      true,
		ParseTime:            true,
	}

	var err error
	for {
		db, err = sqlx.Connect("mysql", dbCfg.FormatDSN())
		if err == nil {
			break
		}
		log.Println(err)
		time.Sleep(time.Second)
	}
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
	return
}

func getLocation(name string) *time.Location {
	loc, _ := time.LoadLocation(name)
	return loc
}
