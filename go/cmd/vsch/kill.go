package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"vostra/go/iferr"
)

func lsof(port int) (pid string) {
	var b []byte
	buff := bytes.NewBuffer(b)

	cmd := exec.Command("bash", "-c",
		fmt.Sprintf("lsof -t -i:%d", port))
	cmd.Stdout = buff
	cmd.Stderr = os.Stderr
	_ = cmd.Run()

	pid = strings.TrimSpace(buff.String())
	if pid != "" {
		fmt.Printf("PID %s is using port %d\n", pid, port)
	}
	return
}

func killPid(pid string) {
	if pid == "" {
		return
	}
	fmt.Println("Killing PID:", pid)
	cmd := exec.Command("bash", "-c",
		fmt.Sprintf("kill -9 %s", pid))
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	iferr.Println(cmd.Run())
}

func killAppPort(port int) {
	killPid(lsof(port))
}
