package main

import (
	"fmt"
	"os"
)

func fatal(v ...any) {
	fmt.Println(v...)
	os.Exit(1)
}
