package main

import (
	"os"
	"os/signal"
	"time"

	"vostra/go/dbhelper"
	"vostra/go/iferr"
	"vostra/go/schapi"
	"vostra/go/scheduler"
	"vostra/go/scheduler/runapp"
)

func init() {
	initConfig()
	initCWD()
}

func initCWD() {
	cwd := os.Getenv("CWD")
	if cwd == "" {
		fatal("CWD env is empty")
	}
	iferr.Fatal(os.Chdir(cwd))
}

func killAppInstances() {
	for i := 0; i < cfg.Scheduler.NumberOfInstances; i++ {
		killAppPort(cfg.App.PortStartFrom + i)
	}
}

func main() {
	// Kill existing server / app using scheduler port and app port
	killAppPort(cfg.Scheduler.Port)
	killAppInstances()
	if *flagKillOnly {
		os.Exit(0)
	}

	startSchedulerAndRunServer()
}

func startSchedulerAndRunServer() {
	db := dbhelper.ConnectAndInit(
		cfg.MySQL.Host, cfg.MySQL.Port, cfg.MySQL.DBName, cfg.MySQL.User, cfg.MySQL.PasswordEnv)
	scheduler.CreateTables(db, cfg.Scheduler.DropTables)

	m := map[string]*scheduler.Scheduler{}
	opt := scheduler.Options{
		NumberOfInstances:     cfg.Scheduler.NumberOfInstances,
		AppPortStartFrom:      cfg.App.PortStartFrom,
		DurationPreparation:   time.Duration(cfg.Scheduler.PreparationSec) * time.Second,
		DurationFullJobQ:      time.Duration(cfg.Scheduler.SleepWhenFullJobQSec) * time.Second,      // 5 sec
		DurationLessJobQ:      time.Duration(cfg.Scheduler.SleepWhenLessJobQSec) * time.Second,      // 1 sec
		DurationNoJobAssigned: time.Duration(cfg.Scheduler.SleepWhenNoJobAssignedSec) * time.Second, // 1 sec
		DurationNoMoreEvent:   time.Duration(cfg.Scheduler.SleepWhenNoMoreEventSec) * time.Second,   // 1 sec
		JobsMaxQueue:          cfg.Scheduler.JobsMaxQueue,
		JobsRemaining:         cfg.Scheduler.JobsRemaining,
	}
	scheduler.StartScheduler(
		opt, db,
		func(appPort int) runapp.Runner {
			return runapp.New(cfg.App.Name, cfg.App.Args, cfg.Scheduler.Port, appPort)
		},
		func(instanceId string, sch *scheduler.Scheduler) {
			m[instanceId] = sch
		})

	quit := make(chan bool)
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)
	go func() {
		<-sig
		killAppInstances()
		quit <- true
	}()
	go schapi.RunServer(cfg.Scheduler.Port, m)
	<-quit
}
