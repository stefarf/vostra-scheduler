package main

import (
	"flag"
)

var (
	flagKillOnly        = flag.Bool("kill", false, "Exit after kill all processes using the same ports.")
	flagWriteConfigOnly = flag.Bool("cfg", false, "Exit after rewrite config file.")
	flagSetNo           = flag.Uint("set", 1, "Set number / index starting from 1.")
)

func init() {
	flag.Parse()
}
