package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"vostra/go/iferr"
)

type config struct {
	MySQL struct {
		Host        string
		Port        int
		DBName      string
		User        string
		PasswordEnv string
	}

	Scheduler struct {
		Port              int
		NumberOfInstances int
		DropTables        bool

		PreparationSec            int
		SleepWhenFullJobQSec      int
		SleepWhenLessJobQSec      int
		SleepWhenNoJobAssignedSec int
		SleepWhenNoMoreEventSec   int
		JobsMaxQueue              int
		JobsRemaining             int
	}

	App struct {
		Name          string
		Args          []string
		PortStartFrom int
	}
}

var cfg config

func isFileExist(filename string) bool {
	_, err := os.Stat(filename)
	return !errors.Is(err, os.ErrNotExist)
}

func initConfig() {
	configFile := os.Getenv("CONFIG_FILE")
	if configFile == "" {
		fatal("CONFIG_FILE env is empty")
	}

	// Read config file if exist
	if isFileExist(configFile) {
		b, err := ioutil.ReadFile(configFile)
		iferr.Fatal(err)
		iferr.Fatal(json.Unmarshal(b, &cfg))
	}

	// Re-write config file
	b, err := json.MarshalIndent(&cfg, "", "  ")
	iferr.Panic(err)
	iferr.Fatal(ioutil.WriteFile(configFile, b, os.ModePerm))

	// Exit if the -cfg flag is set
	if *flagWriteConfigOnly {
		fmt.Println("Exit after writing config file:", configFile)
		os.Exit(0)
	}

	// The update the scheduler and app port based on the -set flag
	if *flagSetNo < 1 {
		fatal("The -set argument must be more than or equal 1")
	}
	set := int(*flagSetNo - 1)
	cfg.App.PortStartFrom += cfg.Scheduler.NumberOfInstances * set
	cfg.Scheduler.Port += set
}
