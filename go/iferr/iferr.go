package iferr

import (
	"log"
	"os"
)

func Panic(err error) {
	if err != nil {
		panic(err)
	}
}

func Fatal(err error) {
	if err != nil {
		fatal(err)
	}
}

func Println(err error) {
	if err != nil {
		log.Println(err)
	}
}

func fatal(v ...any) {
	log.Println(v...)
	os.Exit(1)
}
