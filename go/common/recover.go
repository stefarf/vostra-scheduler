package common

import (
	"log"

	"vostra/go/iferr"
)

type CanRollback interface {
	Rollback() error
}

func RecoverRollbackPanic(tx CanRollback) {
	if r := recover(); r != nil {
		iferr.Println(tx.Rollback())
		log.Println("Rollback for error:", r)
		//debug.PrintStack()
		panic(r)
	}
}

func RecoverAndLog() {
	if r := recover(); r != nil {
		log.Println("Panic:", r)
		//debug.PrintStack()
	}
}
