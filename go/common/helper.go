package common

import (
	"database/sql"
	"encoding/json"

	"github.com/google/uuid"

	"vostra/go/iferr"
)

func IfEmptyUuid(s string) string {
	if s == "" {
		return uuid.NewString()
	}
	return s
}

func StringToSqlNullString(s string) sql.NullString {
	if s != "" {
		return sql.NullString{String: s, Valid: true}
	}
	return sql.NullString{}
}

func JsonMustMarshal(v any) []byte {
	b, err := json.Marshal(v)
	iferr.Panic(err)
	return b
}
