# VOSTRA Scheduler

Utilize the MySQL / MariaDB and implement the kafka-like consumer.

## Download the latest built binary

https://gitlab.com/stefarf/vostra-scheduler/-/tree/main/bin

## Development

1. Run the `run-mariadb.sh` script to start the DB server.

2. Connect to DB server and create `scheduler` schema:

    ```mysql
    CREATE SCHEMA scheduler;
    ```

3. Run the helper scripts:
    - `gorun.sh` to test the scheduler
    - `gobuild.sh` to build the binary
    - `run.sh` to run the binary
    - `godoc-start.sh` and `godoc-open.sh` to start the godoc server and open in the browser

### Tips

#### See all command flags available

Run the `gorun.sh -h` or `run.sh -h`.

#### Rewrite config file

Run the `gorun.sh -cfg` or `run.sh -cfg`.

#### Run multiple sets of scheduler and apps

Run the `gorun.sh -set` or `run.sh -set`.

The default value for the `-set` flag is 1. To run the second set put 2 as the value and so on for the third etc.

## Backlogs

- Rename scheduler to streamer (?)